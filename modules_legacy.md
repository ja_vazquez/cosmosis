# Importing external code into CosmoSIS #

If you have existing code then it may be written as a library of different functions for doing a calculation, or as a single main program.  Our goal is to turn this code into a library to do the calculations we need, and then write an interface to connect it to cosmosis.

### Converting a library ###

Converting a library of functions into cosmosis is typically very simple - the calculation part of your code is just the library functions as they are.

### Converting a program ###

Converting a monolithic program is a little harder, but not much.

Essentially you need to turn your program into one more functions, by moving code around so that the main program is a just a handful of very simple calls to those functions.  For example, those functions might be one to set up the code in general, like allocating arrays or setting constants, another to read in any inputs to the code from files, one to do the main calculation, and one to save the results.

You need to put these functions in separate files from the main program, and change your makefile to compile them separately. The idea is that once we have these separate functions we can connect them to cosmosis.

The Cosmosis team is happy to help with this - just [open an issue](https://bitbucket.org/joezuntz/cosmosis/issues?status=new&status=open).


In the long run you can make a much better and more widely usable module by altering your program so that cosmological inputs to the program (e.g. matter power spectra, n(z), H(z), etc.) are loaded from the DataBlock instead of having a functional form assumed.  Often the easiest way to do this is replacing an offending function with a spline interpolating into arrays loaded from the DataBlock.


### Writing the Connecting interface

You need to write the three functions described on the [modules page](modules) - setup, execute, and cleanup.  The easiest way to do this is examine an existing module.  A listing of the functions you can use is available on the pages for the different languages:


[Writing python modules](modules_python)

[Writing C modules](modules_c)

[Writing C++ modules](modules_cpp)

[Writing Fortran modules](modules_fortran)

