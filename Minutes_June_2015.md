# CosmoSIS Telecon Thursday 4th Jun 2015

Present:

- Mustapha Ishak
- Tom McClintock
- Elise Jennings
- Alessandro Manzotti
- Rene Hlozek
- Scott Dodelson
- Michael Schneider
- Agnes Ferte
- Donnacha Kirk
- Ian Harrison
- Simon Samuroff
- Adam Mantz
- Alex Berreira

Actions:

- Add in Galileon model to the CSL with Alex and Elise
- Improve the plotting/postprocessing tools and their documentation
- Add a shared physical constants header/module
- Add some helper functions to get common cosmological functions
- Implement fast/slow sampling in metropolis


CosmoSIS Version 1.2
--------------------

Joe: Would like to find out what would make it more useful for you. About 3 days ago we put out a new version (1.2) of cosmosis. You can see the changes in the changelog on the wiki. The main changes were in the samplers and the standard library. 

A bunch of new samplers are included:
 (i)   PMC which is very similar to that in Planck and CFHTLenS
 (ii)  Snake sampler - more intelligent than the usual one 
 (iii) List sampler and importance sampler - reanalyse samples with new likelihoods 
 (iv)  Kombine sampler

Elise: Kombine sampler is written by Ben Farr (Chicago). Its an ensemble sampler - like EMCee but better at multimodal distributions and can get evidence. The proposal uses KDE and so is fast and good with mutliple peaks. It handles lots of different parameters very well. Ben works in lensing where you have lots of parameters.

Renee: Thank you very much. I’m definitely going to use it for my axion work.

Joe: There is much better documentation for the existing samplers and the different options they can take. If you want to do clever things with the test sampler or understand what the snake sampler is doing. References to the papers for these samplers too.

Michael: Are these interchangable, so if I change the name of the sampler then does the postprocessing handle it?

Joe: The postprocessor knows about the different types of sampler. The 2 grid based samplers behave in the same way in the postprocessing. The MCMC samplers split into those that have that a weight column, and ones that don’t. So you can postprocess any of them at the moment.

Joe: Remaining changes are a bunch of bug fixes. Some new post processing options. You can now blind all your results with options in the post processing. So you can make sure you don’t get LCDM.

Michael: Does that blinding save some sort of reference file so you’d have to know what machine its on to see the blinding factor.

Joe: No, you have to reprocess.

Joe: Single most requested feature was switching x and y axes. Now available with the “swap” feature. 

Also a selection of new science module: new version of isitGR, code associated with cluster gas fractions and BBN consistency from Adam, from Phil Marshall some strong lensing likelihoods, new version of CAMB that Elise just mentioned, MGCAMB, Niall has added some NICEA code, Lucy Clerkin added nice bias code.

Joe: That’s an overview of the new features. 

Requests from the last telecon. 
------------------------------

Mustapha asked for a CFH lens optimised library. It isn’t in the main branch because it would need to be tested.

Mustapha: It worked fine in the demos, and I think Elise tested.

Elise: It runs well for me. Yaml files are corrected now.


Joe: People asked for tools e.g. pulling down modules from an external repository and reporting errors. There is now a collection of little utlities - python scripts. See the helper page. e.g. to create a full error report - you can run in error-reporter-mode and get all the inputs etc and you could file an issue using that tarball. If you have problems in the future then please use that utility. Then there is a module downloader - if you know of a url then you can run that command and download.

Michael: There is a working model that everything goes in the modules directory. If I’m developing a new module then does it have to sit in there?

Joe: Its the most convenient place to do it because the Makefile will pick it up automatically. There is a flag to the downloader script that renames where its going.

Rene: I did an update from the development version which then removed my separate folder in the modules directory. It was probably my mistake. It was under version control so I don’t know what the mistake was.

Joe: If you can figure out what happened it would be good to know.

Michael: It sounds appealing that there is a script.

Joe: The script always puts things under the modules.

Michael: Each subdirectory module could be in a different repository.

Joe: The prompt also now changes so you know which repository you’re in.

Scott: When you do create your own subdirectory you should create your own repository for it.

Joe: There is another tool that will tell you if there are unsaved changes. Hopefully if you’re managing lots of different things.

Scott: Different to git status?

Joe: The same but walks through the various different repositories.

Joe: The final thing that was requested from the last telecon was ISW-LSS and Michael asked me to write up naming conventions for spectra sections. Sorry that - I haven’t got around to this. Some people asked for a tutorial on CAMB.

Renee: I took a look and it was very useful. Describing whether or not you do need to modify CAMB. Its not up to CosmoSIS to describe the CAMB changes but its useful that you describe what to look out for.

Joe: Great, in that case I’ll put it up on the main page.

Joe: Any questions about anything that’s been updated in v1.2.



Community Update: Alex Barreira
-------------------------------

Joe: Great to hear updates - what you’ve found useful and any particular problems.

Alex Berreira: This is work I’m doing with a bunch of people and Elise - to look if the mass of clusters change with other theories of gravity. So if the mass of cluster changes is it degenerate with gravity model. We looked at different models including Galileon. We looked at 19 cluster profiles. When you go from GR to the 2 modified gravity models, the mass changes by 5%, which is small compared to the errors of current lensing data. For both of the models tiny shifts in mass and concentration can compensate the change in gravity. On the positive side, the lensing measurements are very robust to changes in gravity. For the Galileon model the screening affects just outside the cluster so if you’re using e.g. velocity dispersions you can get a different dynamical mass to GR. I wrote 19 modules (one for each cluster) in CosmoSIS to get the likelihood. In the paper CosmoSIS was used in the 3d parameter space. I have a modified version of CAMB for the Galileon models. I managed to run CosmoSIS with the Planck constraints and I think the constraints were consistent with those I got from CosmoMC. In principle I could add my module. I think all the models are generic parameterisation, so this would be for one specific model.

Action: Add in Galileon model with Alex and Elise

Community Update: Tom McClintock
-------------------------------

Tom: I’m working with Eduardo Rozo at University of Arizona. I’m implementing a whole suite of modules to do cluster counts with optical richness. At the moment I’m finishing up modules that calculate the surface overdensity functions. Mostly focussing on keeping my modules fast and lightweight. I’m looking at a lot of old code and porting it over. CosmoSIS has been really useful. One thing that could be more intuitive is how to make your own plots. When you’re branching off and doing your own thing it gets tough to tell cosmosis to make plots exactly how you want. Overall its been pretty great. I really enjoy and found it really helpful to have the updates to the documentation.

Joe: In addition to documenting the samplers the modules are all better documented too. Are you happy to have your stuff go into the standard library?

Tom: Yes. I’m testing against nbody simulations right now and will let you know.

Scott: Suppose Tom uploads to the cosmosis standard library, how will I know its there and whether I can use it?

Tom: I’m writing the yaml files.

Joe: We turn the yaml files into the wiki.

Community Update: Agnes Ferte
-----------------------------

Agnes: Our main goal is to reproduce the CFHTLenS analysis to constrain modified gravity, using lensing from CFHTlenS and redshift space distortions. The main job is to modfiy MGCamb which is already in CosmoSIS. This is work in progress. It has been pretty straightforward. In the future we would like to add likelihoods. We should check if its the right version. In the future we would like to use DES likelihoods. I had no major problem. For the moment its really great to work with cosmosis.

Joe: You’re working together with Donnacha who’s online - any comments Donnacha?

Donnacha: That’s a great summary.

Renee: I have a general question about timing. Have there been any timing tests of the code or anything relevant to other people as well?

Joe: There is a timing feature. If you set timing = true. We did some work a while back to make that more structured. We want to improve so we get statistics of timings as well. There should be instructions in the parallelisation section of the wiki which would allow you to use OpenMP which would speed things up a lot on a multicore machine. Other than that we can look at the optimisation options in the makefile.

Tom: With regards to the parallelisation, are there any parallisation options you could set in the ini files?

Joe: OpenMP sets an environment variable. For non OpenMP you can set a number of threads as variable  in an ini file. ?? might be good to do that as an action item. Can do your own parallelisation. Anyone else with speed issues?

Scott: Lets get Rene’s precise information. That’s crucial information for us.

Renee: Yes, I can send the slowdowns from my laptop. Yes, I can send that.

Community Update: Ian Harrison
------------------------------

Ian: I’ve been extending some work Joe did initially on extending forecasting constraints Joe did for SKA and optical surveys like DES and Euclid and we’ve been comparing with Fisher code with Stefano Camera. The place we’re looking to extend CosmoSIS is to look at cross correlations between the two experiments. For the wish-list for cosmosis, I’d add to what people have said would be plotting, and foregrounding the postprocessing - to make it clear that the postprocessor even exists.

Community Update: Renee Hlozek
------------------------------

Renee: The development is ongoing. We’re working to get our module a lot cleaner more attractive than it was, and make sure we can include axions as well as neutrinos. Last time we had axions finished completely. Once we are satisfied it is running fast enough we will move to the standard library. The modularisation appeared to slow things down. We had some integration routines so we were passing vectors of coefficients but I’m not sure that was helping. I’m trying to check where the slowdown was happening. I found that most of the slowdown happened when I moved to a cluster. The timing tests say how long camb takes, and I wanted to know how long each of my new modules were taking so I wrote new bits for that.

Community Update: Adam Mantz
------------------------------

Adam: This is mainly an exercise for me to learn something about CosmoSIS. I had some code for cluster mass fractions. I went ahead and imported it and its now part of the standard library. I have a lot of questions. I think the most important thing is to have a standard function call to get distances instead of having to pull down lots of different functions and have them interpolate them separately.

Joe: To have functions to get cosmlogical quantities.

Adam: Yes. I used the same physical constants that CAMB uses. I think there’s no central place to say what G is, how large a Mpc is etc. If there were such a place that would be an extra layer of robustness.

At some unspecified time in the future I’d like to put my number counts code in. It would be great to coordinate with Tom.

Joe I’ll email you each other’s email address

Community Update: Michael Schneider
-----------------------------------

Michael: Very briefly, I haven’t touched it since there was an LSST theory meeting at the beginning of april I’ve been working on 2 things with the intention of submitting to the standard library - all those possible cross correlations between lensing and large scale structure are not in CosmoSIS. I’ve realised my history is really messy so I’m embarassed.

Joe: There is a rebase command.

Michael: I edit shear-spectra so its now a misnomer because it has the large scale structure as well.

Scott: Where is that? Useful to put in DESC repository?

Michael: I think a fairly basic feature, so standard library.

Also working on DLS tomographic cosmology which should be out in a few weeks. It would be great to have a halo model covariance matrix.

I've been making my own theory plots python file. You probably don’t want my hacked up version of the cosmology theory plots, but might be worth making it possible to add your own theory plots in an additional file.

Exposing CAMB/CLASS Features
----------------------------

Alessandro: I was working on neutrinos in CAMB but not all the neutrinos are in there, working on CLASS now which has more. If someone has hacked somehow CAMB to change in a way that is not the usual cosmosis way that we could start with. If you want to change something using the ini file but it's not there. 

Feature Requests and Ideas
--------------------------

Joe: Any other feature ideas?

Mustapha: In CosmoMC there is fast-slow method that is good for planck. It reads the nuisance parameters and cosmology parameters in a different way which speeds things up.

Joe: There is a branch at the moment with the initial parts of the fast-slow sampling started. At the moment it is just figuring out which parameters are fast and which are slow. Its good to hear that’s a priority for people.

Next Time
---------

Joe: I will do a doodle poll for the time of the next telecon to simplify things.
