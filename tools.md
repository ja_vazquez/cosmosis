#CosmoSIS Tools

Several tools come with CosmoSIS that can help you manage additional modules or get help with problems.

The [CosmoSIS Updater](tools/update_cosmosis) checks for and/or switches to different versions of CosmoSIS.

The [CosmoSIS Error Reporter](tools/cosmosis_error_report) collects together information to submit with an issue or to send to someone.

The [CosmoSIS Module Downloader](tools/cosmosis_get) downloads a module from an internet repository (for example on GitHub or Bitbucket) into a local directory.

The [CosmoSIS Module Creator](tools/cosmosis_new_module) walks you through creating a new CosmoSIS module from scratch including setting up a new online repository to safely keep it in.

The [CosmoSIS Change Checker](tools/cosmosis_check_modules) looks for modifications to cosmosis or user-created repositories that might need to be saved or pushed to a hosting website.
