Author(s) | Title | Reference
----------|-------|----------
Zuntz et al | *CosmoSIS: Modular cosmological parameter estimation* | http://adsabs.harvard.edu/abs/2015A%26C....12...45Z
Barreira et al | *Galaxy cluster lensing masses in modified lensing potentials* | http://arxiv.org/abs/1505.03468
Ma et al | *Probing the diffuse baryon distribution with the lensing-tSZ cross-correlation* | Submitted to JCAP
Dark Energy Survey et al | *Cosmology from Cosmic Shear with DES Science Verification Data* | http://arxiv.org/abs/1507.05552
Park et al | *Joint Analysis of Galaxy-Galaxy Lensing and Galaxy Clustering: Methodology and Forecasts for DES*  |http://arxiv.org/abs/1507.05552
Jennings & Wechsler | *Disentangling redshift-space distortions and nonlinear bias using the 2D power spectrum* | http://arxiv.org/abs/1508.01803
