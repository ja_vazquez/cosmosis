## Auto-Install _CosmoSIS_ in 4 easy steps!

We have an installation script which installs cosmosis and all its dependencies that works on 64 bit **Mac OSX Mavericks** (10.9) and **Scientific Linux 6** or its equivalents (**CentOS, Redhat Enterprise**).

Everything this script installs is put in a single directory which can be cleanly deleted and will not interfere with your other installed programs.


Other platforms will be supported in future, please contact the _CosmoSIS_ development team to let us know of your interest and platform.  The instructions here will not work for other platforms - please see the other set of instructions for [installing on other platforms](https://bitbucket.org/joezuntz/cosmosis/wiki/Manual%20Install).

If you are installing CosmoSIS on the University of Chicago cluster, Midway, please follow the instructions [here](Midway).


## 1  Install prerequisites (if necessary; typically these will already be available)

* [Git](http://git-scm.com) version control system (to see if git is installed, type "git help" at the prompt to see if you get the usage message)
* [Curl](http://curl.haxx.se) (used to automate downloading of _CosmoSIS_) (to see if curl is installed, type "curl --help" at the prompt to see if you get the usage message)
* **On OSX, Install [Xcode](https://developer.apple.com/xcode/) and xcode command line tools <---- DO NOT IGNORE THIS LINE**
```      
#!bash
$> sudo xcode-select --install
```
If you already have xcode installed you might get the following error message when you try the above: "Can't install the software because it is not currently available from the Software Update server." - if so then click "OK" and continue with the below.
            
## 2 Download and execute the [bootstrap script](https://bitbucket.org/joezuntz/cosmosis/downloads/cosmosis-bootstrap) by running the following lines.  Make sure you are running the _bash_ shell

(You can check whether you are running the _bash_ shell by typing "echo $SHELL" at the prompt.)
```
#!bash
$> curl -L --remote-name https://bitbucket.org/joezuntz/cosmosis/downloads/cosmosis-bootstrap
$> chmod u+x cosmosis-bootstrap
$> ./cosmosis-bootstrap -d <new desired target directory e.g. "cosmosis">
$> cd <the new desired target directory that just got created>
```

**Note**: the **-d** option means you will be downloading the Planck and WMAP data - it may take about an hour.  For a quicker start leave it out (some demos will not work).
If you want to get collaboration-specific modules you can add "-e <your collaboration name>" e.g. for DES: "-e des".


## 3  Set up the _CosmoSIS_ environment (this must be done every time you use the program)

```
#!bash
$> source config/setup-cosmosis
```

## 4  Build _CosmoSIS_ libraries and included modules.

```
#!bash
$> make
```

Now you can go back to the [cosmosis home page](https://bitbucket.org/joezuntz/cosmosis/wiki/Home) and try the demos


## Notes

A note for people who understand git: this procedure leaves you in a "detached head" state at the latest version.  You can get to the bleeding edge with: git checkout master