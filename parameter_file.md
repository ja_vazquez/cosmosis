# Page under construction - sorry #

# Building your pipeline with a parameter file

A single run in CosmoSIS is specified with at least two input files: the parameter file, which describes the pipeline, sampler, and expected outputs, and a [values file](values_file), which describes the parameters put into the beginning of the pipeline.  This page describes the parameter file.

# Overview

Like the other CosmoSIS input files, the parameter file is in the [ini format](https://en.wikipedia.org/wiki/INI_file).  It consists of sections, marked by [square brackets], and parameters set in the form name = value within each section. The spaces around the "=" are optional.

Comments are marked by semi-colons.

## Example

Here's an example parameter file from demo 5, with some of the comments removed, just to show the structure:

```
#!ini

[runtime]
sampler = emcee
root = ${COSMOSIS_SRC_DIR}

[emcee]
walkers = 64
samples = 400
nsteps = 100

[output]
filename = demo5.txt
format = text
verbosity= debug

[pipeline]
modules = consistency camb jla riess11
values = demos/values5.ini
extra_output =
likelihoods = jla riess
quiet=T
debug=F
timing=F


[camb]
file = cosmosis-standard-library/boltzmann/camb/camb.so
mode=background
feedback=0

[jla]
; JLA needs quite a lot of parameters telling it where
; data files are ...
file = cosmosis-standard-library/supernovae/jla_v3/jla.so
data_dir = cosmosis-standard-library/supernovae/jla_v3/data
data_file = jla_lcparams.txt
scriptmcut = 10.0

```

We now go through each of these sections in turn to see what they contain.

# The [runtime] section

The runtime section defines some very basic facts about running the code.  It is short and contains just two possible options:

```
#!ini
[runtime]
sampler = emcee
root = ${COSMOSIS_SRC_DIR}
```

Parameter  | Meaning
------------- | -------------
sampler  | Selects which sampler to use to explore the likelihood. See the [front page](Home) for a list. e.g. ``sampler=metropolis``
root  | Sets the base directory to look for modules relative to.  Modules are selected by choosing the ``file`` option (see below).  That file is relative to this directory (unless they are absolute paths starting with "/"). Default is the cosmosis main directory (so the setting shown above is the same as the default).


We'll discuss in the Special Features section below the use of the environment variable COSMOSIS_SRC_DIR in the ``root`` example.

# The [pipeline] section

The pipeline section defines the series of steps that go into building your likelihood calculation.  Here's an example from [demo 14](Demo14):


```
#!ini

[pipeline]
modules = consistency bbn_consistency camb fgas
values = demos/values14.ini
likelihoods = fgas
extra_output = cosmological_parameters/yhe
timing=F
debug=F
priors=

```

Parameter  | Meaning
------------- | -------------
modules  | The main list of modules to make up the pipeline. Each module is run in the order given here. The name simply refers to another ``[section]`` elsewhere in the file, and can by anything. Expected likelihood outputs are given in the ``likelihoods`` parameter and inputs in the [values file](values_file).
values  | The name of the other required input file, the [values file](values_file), which describes the input parameters to the pipeline - their values and ranges.
likeilhoods  | Expected likelihoods to be output by the pipeline.  CosmoSIS will look in for a section in the pipeline's data block called ``[likelihoods]`` and a parameter in that section called ``NAME_LIKE``, e.g. in this example ``FGAS_LIKE``.
extra_output  | A list of derived parameter to be saved in the output file along with the inputs and the likelihoods, in the form section/name.
timing  | if set to "T", print out the time taken by each module in the pipeline.
debug  | If "T", if there is an error print out as much debug information as possible.
quiet  | If "T", suppress printing out much of the normal code output to screen. Things saved to the output file are unaffected.
priors  | If set, look in the named file for a collection of additional parameters to use.  See [priors file](priors_file).

# The sampler sections

For whatever sampler you choose in the ``runtime`` section there should be a section with the same name in the file that specifies the options for that particular sampler.  Different samplers have different options, and you can look through the list of samplers on the [home page](Home) to see what options they all take.

# The [output] section

The output section describes where to save the results of the sampling. Not all samplers use this facility - for example, the test and maxlike samplers produce only a single value and so do not need an output file. Example:

```
#!ini

[output]
filename = demo5.txt
format = text
verbosity= debug
```

Parameter  | Meaning
------------- | -------------
filename  |The name of the output file
format  | The format to save in. At the moment only ``text`` is supported, though we are looking into database and other options.
verbosity | Choose from "silent", "quiet", "gentle", "standard", "noisy", "debug", and "highest" to get different amounts of output. Not all the printing code uses this mechanism so we need to improve it.

# The module sections

Every module in the pipeline chosen in the ``[pipeline]`` section must have a corresponding section somewhere else in the file.  There is only one required parameter in the section for each modul, which sets the file that that module should use, e.g.:

```
[jla]
file = cosmosis-standard-library/supernovae/jla_v3/jla.so
```

Parameter  | Meaning
------------- | -------------
file  | the name of the python file or shared library .so file of the module you want to run. This is relative to the ``root`` set in the ``runtime`` section unless it starts with a "/".
other parameters | any other parameters that are set here are accessible in the module itself when its setup function is run, so you can also configure any other options, file paths, etc. here.  See the pages on writing modules for more info.


You can have extra sections in the file which are not currently used in the pipeline if you want.  This is useful if you are experimenting.

# Special Features

There are various special features that are available to help you write more flexible and re-usable ini files.

## Environment variables

Ini files can use environment variables in the form ${VARIABLE_NAME} anywhere in the text.  These are pasted in as pure text, so they can go anywhere - in parameter names, value names, or section names.  In the example above the root is set to ${COSMOSIS_SRC_DIR}, for instance.

Beware: if the environment variable is not found you won't get any kind of error message - the variable will just be 

## Inheritance

## The [DEFAULT] section and variable substitutions