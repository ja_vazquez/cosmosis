#Modifying modules

A very common thing to do is modify an existing piece of code to do a new task or implement a new theory.  For example, you might make add modified gravity or some other piece of new physics to a calculation module.

## Steps 

1. Create a new repository for your project using using the command
```
#!bash
./cosmosis/tools/create-repository.py NAME_OF_PROJECT
```

Follow the instructions that appear


2. Copy the existing module into a subdirectory your new repository:
```
#!bash
cp -r cosmosis-standard-library/EXISTING_MODULE  modules/NAME_OF_PROJECT/NAME_OF_MODULE
```


3. Add the copied files to the repository:
```
#!bash
cd modules/NAME_OF_PROJECT/NAME_OF_MODULE
make clean  # unless module is in python
git add .
git commit -m 'Initial commit of files for NAME_OF_PROJECT'
```

4. Make any modifications you want to the code.  Then save the results:
```
#!bash
#  ... lots of work here ...
git commit -am 'Made my changes'
git push
```

You can now use your modified module by changing the path in the ini file to point to it.
