[Introduction (Scott Dodelson)](pdf/cosmosis_may2014_intro.pdf)

[Under the hood of CosmoSIS (Marc Paterno)](pdf/cosmosis_may2014_under_the_hood.pdf)

[Adding a module: CFHTLens example (Joe Zuntz)](pdf/cosmosis_may2014_cfhtlens_example.pdf)

[Adding a module: Talking to CosmoSIS (Douglas Rudd)](pdf/cosmosis_may2014_cosmosis_api.pdf)

[Adding a module: Getting Acknowledgement (Sarah Bridle)](pdf/cosmosis_may2014_getting_acknowledgements.pdf)
