
# Creating a new module #

This section describes the steps necessary for creating the code and structure of a new module.  Before starting, CosmoSIS module developers should consider the audience for their module in order to choose a suitable repository location (a page describing this in more detail is forthcoming).


## Making a repository for your module ##

Run this command from the main cosmosis directory:

cosmosis-new-module --help

It will help you set up a new collection of modules for you to write your own modules into.


## Writing your module ##

A module has **two parts** - the bit that does the actual **calculation**, and the bit that **plugs in** that calculation to CosmoSIS.  Very small modules may not need to distinguish between these two bits, but in general you should keep them distinct.


## Calculation ##

You might have code already to do the calculation part, or want to start afresh or modify an existing module.  Take a look a the specific instructions for each case:

[Converting external code into a module](modules_legacy)

[Writing a module with new code from scratch](modules_new)

[Making changes to an existing module](modules_modify)

## Plug-in ##


Once you have the code to do your calculation, you need to write the plug-in or *interface* to connect to CosmoSIS.

This consists of three simple functions: **setup**, **execute**, and **cleanup**

## SETUP ##

The setup function is called once per processor, and reads options in from the CosmoSIS ini file, loads any data needed by the module, and can allocate memory or set any global options.

Anything set in the setup function is maintained for later when execute is called.

## EXECUTE ##

The module execute function is called each time we have new cosmological parameters.  It loads parameters and data created by previous modules, runs its main calculation, and saves whatever it produces.

## CLEANUP ##

The cleanup function is optional and is run once per processor at the end of the sampling.  It frees any resources like memory used by the module.

## Languages ##

The exact form your functions take depends on which language your module is in:

[Writing python modules](modules_python)

[Writing C modules](modules_c)

[Writing C++ modules](modules_cpp)

[Writing Fortran modules](modules_fortran)
