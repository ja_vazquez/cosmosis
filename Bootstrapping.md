This page explains how to download the essentials you need on your system *before* you can install cosmosis. This includes standard libraries like gsl, cfitsio, fftw, gcc, lapack, mpich, nose, numpy, pyfits, python, scipy, sqlite, pyyaml and data from Planck and WMAP. These are all installed locally so cosmosis can use them, and they do not interfere with any other libraries you may already have installed for other purposes. 


The script `cosmosis-bootstrap` is used to download the pre-built
software products and data files upon which _CosmoSIS_ depends. The
script can be downloaded [by clicking on this
link](https://bitbucket.org/joezuntz/cosmosis/downloads/cosmosis-bootstrap).
In order to use the script, you'll need to open a shell session in the
directory into which you have downloaded the script. You can put this
script anywhere you like; it will only be used for the download, and can
be removed after you have executed it.

Change the permissions of the script with
```
#!bash
$>  chmod +x ./cosmosis-bootstrap 
```

Check things are working on by executing the script with option `-h`, which gives help on using it. 
```
#!bash
$> ./cosmosis-bootstrap -h
```

The output of `./cosmosis-bootstrap -h` will be the following help:

```
#!bash
$> ./cosmosis-bootstrap -h
USAGE: cosmosis-bootstrap [-p] [-w]  <manifest> <target-directory>
       cosmosis-bootstrap installs the software upon which CosmoSIS depends.

OPTIONS:
    -h: Print this help and exit
    -p: Install the PLANKCDATA package (default does not install PLANCKDATA)
    -w: Install the WMAPDATA package (default does not install WMAPDATA)

'manifest' is the file containing the names and versions of the products
to be installed by the bootstrap process.

'target-directory' is the directory under which everything is
installed. It will be created if necessary.
```

You need to download an additional file (the appropriate manifest) which is specific to the operating system you are running. Currently supported are Scientific Linux 6.x (and other RedHat Enterprise Linux 6.x derivatives), and OS X 10.9 ("Mavericks", referred to as "Darwin 13"). The manifest files can be downloaded through the links below.

* [SLF6 manifest for _CosmoSIS_ v0.0.1](https://bitbucket.org/joezuntz/cosmosis/downloads/slf6-files.v00_00_02)
* [Mavericks manifest for _CosmoSIS_ v0.0.1.](https://bitbucket.org/joezuntz/cosmosis/downloads/d13-files.v00_00_02)

You should download the relevant manifest to the same directory into
which you downloaded the script. The manifest may also be deleted after
you have executed the bootstrap script.

For example, to install all the needed packages on your Mavericks
machine, create a directory named `products` under your home directory.
(You may create the directory anywhere you want, and name it whatever
you want; in this example, we use `products` under `$HOME`.) Then run
the bootstrap script (from the directory in which you have downloaded
the script and the manifest). This will take a few minutes to run, if
you are on a fast network connection. The example below uses the `-p`
and `-w` flags to install the PLANCKDATA and WMAPDATA products. If you
are on a poor network connection, you may not want to install these
products; they each require downloading a file of about 1GB.

```
#!bash
$> mkdir $HOME/products
$> ./cosmosis-bootstrap -p -w d13-files.v00_00_02 $HOME/products
```

In order to start making use of the products you have now installed, you must first source the script that makes the product management commands (e.g. `setup`) available. You can do this by hand in each shell session, or put the command in your `.bash_login` or equivalent. The command to use is:
```
#!bash
$> source $HOME/products/setups
```

You can verify that everything is working by using the command that lists all the packages you have installed:
```
#!bash
$> ups list -aK+
```

Note that the installation of products only needs to be repeated when
new products, or new versions of existing products, are added to the
system. This will always be associated with an incrementing of the
version of the manifest files.

The sourcing of the `setups` script needs to be done in each shell
session for which you wish to use the products you have installed.

Now you are ready to [install _CosmoSIS_](Build.md)