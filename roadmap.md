#Plans for future features and work

## Demos
 - Move the demos into the standard library
 - Demo some additional science modules

## Documentation
 - Add reference documentation for cosmosis core features

## Samplers

 - Remove vestigial PyMC code
 - Finish the Minuit sampler

## Standard Library

 - Make it possible to run two copies of CAMB by moving options into an settings space
 - Expose more options form camb and class
 - Add Mead et al NL/Baryon power code
 - cluster cosmology tools

## Other

 - Improve provenance tracking using DB-style output
 - HDF5 output?
 - Programmatic access to computed cosmological results

## Restructuring

- Consider moving the demos into the standard library directory
- Make it easier to update and keep track of dependencies


## Features already in the development branch

- See the [development](development) page for info on changes already included in the development version of cosmosis

## Long-term
 - Oculus Rift support

