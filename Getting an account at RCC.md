---------

The CosmoSIS workshop has access to a local cluster, Midway, through the KICP and the Research Computing Center.  For information on the cluster and available software, see its [documentation](docs.rcc.uchicago.edu).

Temporary accounts will be available for CosmoSIS workshop participants.  Users who expect to use Midway over the long term and have a collaborator at the University of Chicago (Scott Dodelson in particular) may find it useful to set up a permanent account at the Research Computing Center, however this can take up to a day to complete.  Instructions for both options are given below.

# Use a temporary account #

The RCC provides temporary guest access to the Midway cluster though a set of USB devices known as Ubikeys.  Obtain one of these from Douglas Rudd and follow the instructions below to log onto Midway. 

![Yubi key instructions.png](https://bitbucket.org/repo/KdA86K/images/1204856477-Yubi%20key%20instructions.png)

# Request an account at RCC #

## 1. Apply for CNetID ##

First fill out the [general user account request form](http://rcc.uchicago.edu/user_documentation/general_user_account_request.html).

The RCC uses the University of Chicago [CNetID](cnet.uchicago.edu) for authentication on its machines.  If you are already affiliated with UChicago you already have a CNetID and should enter it on the above form.

If not, the RCC can request a CNetID be created for you.  Simply enter your date of birth in the "Summary of Work" field (this is necessary to guarantee a unique id is reserved for you).

In particular, fill out the form this way, where you should fill in your own name and DOB instead of the square brackets:

* Name: [John Smith]
* Title or appointment: guest
* Department: 
* PI Account Name: sdodelso
* List of software: CosmoSIS 
* Summary of work: My DOB: [January 1st, 2000]

*N.b. At DOB - careful not to fill out an EU-style date by accident!*

## 2. Claim your CNetID ##

Wait for the confirmation email that you can claim a CNetID and [claim your CNetID](https://cnet.uchicago.edu/claimacct/index.jsp) with the **ChicagoID** you received in this email.

You will have to choose an ID and a password after answering some questions. Once you have a CNetID, you should reply to the email (to user-account-request@rcc.uchicago.edu) in which you should indicate which CNetID you chose.

## 3. Go ahead and **ssh** ##

After than you will receive another confirmation email that your **account at the RCC** is opened and added to the pi-sdodelso group. After 30-60 minutes you can ssh into *midway* like this:

```
#!bash

$> ssh [CNetID]@midway.rcc.uchicago.edu
```

For more information about the RCC see here: http://docs.rcc.uchicago.edu/introduction.html.