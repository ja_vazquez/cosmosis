# Where to put CosmoSIS-compatible code

This wiki page discusses places you might want to put your CosmoSIS-compatible code. We don't like to see wheels being re-invented. We really like the idea of cosmology codes being publicly available, and fully compatible with each other, hence CosmoSIS. So we encourage you to put your code publicly available too, but we also understand this isn't always practical, so this page describes a range of possibilities.

# Repositories

When you want to write new code to run with CosmoSIS then you will want that code to be safe. Repositories are very good for this, so for the rest of this page we assume you want to put your CosmoSIS-compatible code into a repository. CosmoSIS is distributed using a version control system repository called [git](http://git-scm.com). 

You can use whatever type of repository you want to. We really like git and public repository hosts such as [BitBucket](http://bitbucket.org) or [GitHub](http://github.org)  because they provide a number of useful development and documentation features. How you want to set up your repository depends on who you are sharing your code with, and who will support it. The automatic script below guides you to using BitBucket.


# Creating repositories

There is a script in CosmoSIS to automatically create new repositories for you, and add them to the right cosmosis Makefile.

For example, to create a new repository for FAB analysis, with a new module in it called number_density:

From the main cosmosis directory, run: 
```
#!bash
cosmosis-new-module fab number_density
```

And follow the instructions that appear.  You will create a BitBucket repository for your new modules.  You can also manually create repositories and add them to the CosmoSIS Makefile if you want.


# Writing code for just you and your collaboration

When you create a repository you can choose to make it public or private.  If you are making code for a wider collaboration and want to keep it private then you will need to give collaborators access to it using the controls on the host you use (e.g. BitBucket or GitHub).

Nothing precludes modules in this category from becoming more widely accessible at a later time, e.g. when you publish a paper and want to share your code, or when your collaboration releases data.


# Writing code that the world can access

If you want to keep managing your own code, or your code is not suitable for importing into the cosmosis-standard-library, but you are happy to make it publicly available then that's really encouraged. Please follow the instructions above but make your repository public.

# Writing code for incorporation into CosmoSIS

If you would like to add a feature which you think would be very popular among users and not have any controversial development issues, then you could consider writing some code that might be added into CosmoSIS at some stage. Please get in touch with as you're getting started, so we can help figure out how this might fit in. Once the code is in CosmoSIS it will be maintained by the CosmoSIS core developers, but you can of course supply updates. 

CosmoSIS development follows the Bitbucket fork and pull-request model to ensure code is properly reviewed and tested prior to being accepted into the official repository.

The CosmoSIS code you installed, before running the demos, lives in two separate git repositories:
1. "cosmosis core" https://bitbucket.org/joezuntz/cosmosis/wiki/Home 
and 
2. cosmosis-standard-library http://bitbucket.org/joezuntz/cosmosis-standard-library

First, create your own fork (a local development copy) of either [cosmosis](https://bitbucket.org/joezuntz/cosmosis) or the [cosmosis-standard-library](https://bitbucket.org/joezuntz/cosmosis-standard-library) repository as shown below:

![standard-library-fork.png](https://bitbucket.org/repo/KdA86K/images/2090731148-standard-library-fork.png)

![standard-library-fork2.png](https://bitbucket.org/repo/KdA86K/images/1165023036-standard-library-fork2.png)

If you don't want your development version to be visible to the rest of CosmoSIS before it is ready to submit, be sure the "Inherit repository user/group permissions" checkbox is unchecked.

You will then need to change your local copy of CosmoSIS or the CosmoSIS Standard Library to point to your development fork.  From the main cosmosis directory, do either:

    git remote set-url origin https://bitbucket.org/YOUR_USERNAME_HERE/cosmosis

or:

    cd cosmosis-standard-library
    git remote set-url origin https://bitbucket.org/YOUR_USERNAME_HERE/cosmosis-standard-library


depending on which you forked.  When you push/pull it will now be from the forked version.

Make your changes to the CosmoSIS core code or develop your module following the instructions on the CosmoSIS Wiki and commit changes as necessary. When you feel your changes are ready to be reviewed by CosmoSIS developers, submit a pull request and someone will be assigned to review your change or referee your module for inclusion. That author may have questions, perform tests, and make or ask that you make changes to your module in order to ensure it meets the criteria for inclusion in the Standard Library. Be sure to include plenty of detail in the pull request description to help the referee evaluate your contribution.

![standard-library-pull-request.png](https://bitbucket.org/repo/KdA86K/images/3272014482-standard-library-pull-request.png)

![standard-library-pull-request2.png](https://bitbucket.org/repo/KdA86K/images/1302756712-standard-library-pull-request2.png)

For more information, see the [BitBucket documentation on pull requests](https://confluence.atlassian.com/display/BITBUCKET/Work+with+pull+requests)