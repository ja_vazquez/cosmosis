# Adding a new likelihood#
## A python tutorial ##

The easiest way to get started adding a new CosmoSIS module is to use python to write it.

In this short tutorial we'll show how to add a new likelihood module in python - we will use the example of time delay measurements from strong lenses

## Step 1: Creating a space to store the module ##

You might just want to keep your module to yourself, in which case you don't need to do anything special to store it somewhere - any old directory will do.  But we'll assume that you would eventually like to share this with the rest of the world by having it included in the cosmosis standard library.

### Step 1A: Forking cosmosis-standard-library ###

### Step 1B: Making a new branch ###

## Step 2: The physics part ##

## Step 3: Connecting to CosmoSIS ##

## Step 3: Running a test sample ##

## Step 4: Sampling parameters ##

## Step 5: Sharing your module ##
