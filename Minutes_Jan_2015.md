# Minutes from the January 29th 2015 CosmoSIS open telecon #

## Present ##

Marc Paterno (Fermilab)

Jason Dossett (UTDallas)

Alessandro Manzotti (UChicago)

Katarina Markovic (UPortsmouth)

Donnacha Kirk (UCL)

Renee Hlozek (Princeton)

Mustapha Ishak (UTDallas)

Elise Jennings (UChicago)

Andrew Liddle (UEdinburgh)

Andrew Hearin (UChicago)

Simon Samuroff (UManchester)

Niall MacCrann (UManchester)

Michael Troxel (UManchester)

Sarah Bridle (UManchester)

Michael Schneider (LBL)

Scott Dodelson (UChicago)

Saba Sehrish (Fermilab)

Agnes Ferte (UEdinburgh)

Doug Rudd (UChicago)

Joe Zuntz (UManchester)

... please add your name here / email Joe/Sarah to ask them to do it for you - thanks!


## Actions ##
 - Add the CFHTLenS optimized galaxy sample likelihood
 - Write up the current and proposed naming conventions for spectra
 - Add a tool to pull down module(s) from an external repo
 - Add a tool for error reporting, perhaps based on yt
 - Add a BAO likelihood
 - Look into ISW-LSS cross-correlation likelihood


## Introduction ##

Joe:

Many thanks for coming!  As well as connecting as many codes as possible so we can compare, test, and use them all in one place, we want to make cosmosis development as open as possible.  So community conversation about what would be useful is very important for us.

## Current CSL development and ideas ##


We'll start with an overview of development that's happening by the cosmosis team in branches of the repository.  We have two main ongoing pieces of work in the standard library - MGCAMB, somewhat related to isitgr but with different formalism and parametrization.

We also have a branch for CLASS development, using the python bindings:

Katarina: How advanced is CLASS?  We are working on this too?

Joe: Very preliminary.

We've been putting up new releases of CAMB in as it arises. How useful is it to have older versions?

Elise: Very useful.

Andrew H: Very useful to compare results

Joe: There would be a slight overhead in disc space and then would have multiple directories. 

Jason: The newest version has a lot more output e.g. on transfer functions for the matter power spectrum. Its going to require changes to the interface.

Donnacha: Really helpful to have a likelihood module for each major dataset. Is there LSS/BAO?

Joe: There is an experimental WiggleZ module  but some issues with non-linear. Contributions welcome.

Michael: For different probes. What naming conventions in the Block? 

Joe: General approach is that first come sets the preference. CAMB outputs a lot so sets a lot of precedents. Anything specific?

Michael: Would like the galaxy mass power spectrum variable with luminosity. I don’t know where to store that or how to handle that. Different 1 halo terms? I’d rather have that structure created for me. 

Joe: We have various places where there are multiple spectra in the same category e.g. xi+ for different shear tomographic bins. We tend to keep that in one section. 

Michael: You have different input power spectra for shear for pk and GI, II. If not computing IAs store in one place. If we are store in a new section. Would be nice to have some naming convention. 

Joe: Will write up the naming conventions we already have and suggest something.

Mustapha: Catherine Heymans has put on the CFHTLenS website the red galaxy samples and the optimized red sample very recently. We used them in our recent paper on MG parameters and intrinsic alignments. Could they be added?

Jason: The red galaxy sample is the foreground and blue is the background so you have to take that into account when you compute the functions. Will require some changes to the code. Would be good to have because can enhance the detection of the IA signal. 

Mustapha: It is when you use the red optimized galaxies that you get the 95% CL detections for IA. Catherine made them available earlier this month. When we try to use the files as they are defined, the names of the files as they gave them may not work automatically in the software. This needs to be taken into consideration.


## Community development ##

### IsItGR ###


Mustapha: We updated to 2013 CAMB. Joe already put a first version of IsitGR in the CosmoSIS standard library. A week ago Tharake packaged IsitGR2014. There are two versions: IsitGR2014 and IsitGR_bin_2014. IsitGR2014 uses a functional form for the MG parameters. The dependence on z and scale are given as a function. The binned version uses a binned parameterisation. It uses 2 z bins and scale bins for the MG parameters. We put the module.yaml - there is some explanation there. We followed closely what Joe did originally. There is more information on the IsitGR website. Also reference to two technical papers. We have several versions. How to make them available?

Joe: Very keen to have them go out with CosmoSIS in the standard library. As to which version. Does the new version make the old one obselete?

Jason: The new version that changes the CAMB - there aren’t very significant changes to CAMB. As far as the CAMB backbone IsitGR remains pretty much unchanged. 

Mustapha: Certainly there should be two versions: (i) the functional form (ii) the binned form. The most recent one IsitGR2014 and IsitGR2014_binned should replace the earlier one.

Jason: The biggest change is that I changed the way I call the IsitGR functions. That’s now all in a module at the top of equations.f90. If you want to change the way the IsitGR parameters evolve you do it at the top of the code instead of throughout the code. If you wanted to use f(R) and code it in, you have to change the code just in 1 place. Its much easier to change in the latest version. I agree with what Mustapha is saying. Could build an interface which is choosing the functional form for these parameters. Then have it intelligently decide how to use it.

Mustapha: They are different and both should be there. We should use the most recent one for IsitGR i.e. IsitGR2014, and then add the binned one. Then later on if we find there is a good way to merge the two versions we can update accordingly.

Joe: I will replace the one I did and put these two in.

### Axions ###

Renee: This is ongoing work with others. We have a modified version of CAMB that is treating the axions that can behave either like DM or DE because it makes an effective sound speed for the fluid. Our goal is to release it as an additional CAMB module that you can substitute out. As CAMB changes we need to release notes on how people can do this themselves or keep up to date. The main things that change in CAMB is recfast and equations. Would like to try using different samplers to probe this degenerate parameter space. What I really like is that I can switch in different samplers in CosmoMC. What I would like to see in the future is more samplers. Main issues are how we incorporate version control on our side, and improvements on the samplers. 

Joe: Any particular samplers?

Renee: This is a very multi-modal space. We have issues with multinest and with standard metropolis hastings. We’ve been using emcee. Would like to see other samplers, but not specific. Maybe Hamiltonian

Joe: Population Monte Carlo?

Renee: Definitely.

Joe: Just like we can do with CAMB you can have different versions. Some kind of tool to say there is this repository here - now turn it into a module.

Renee: Yes. 

Joe: A script or a tool to do that would be really good.

Renee: Yes, like for the power spectrum, you want to be able to plug and play with someone else’s modifications. 

?: If we do that we would want to have a conflicts script to find things that are going to conflict. Should be some type of input. 

### Rozo Cluster Codes ###

Simon: I’ve been working with a bunch of code from Eduardo Rozo which computes observables for cluster cosmology. I’ve been breaking it into a form for CosmoSIS. Power spectra, mass functions, Delta Sigma. I’m fairly early on in getting that to work but it should be there eventually. 

Sarah: Anywhere on the wiki where we can see the various mass functions that exist next to each other. 

Elise/Joe: There is a section on mass functions on the wiki

### DLS Galaxy-Galaxy Lensing ###

Michael: We have measurements from DLS in galaxy redshift bins and use clustering to constrain linear bias and the lensing to constrain the cosmology. I thought I would use the CosmoSIS standard library to do this. We need some other power spectra. Most of the implementation exists already in the shear module. I need to add a galaxy mass cross power spectra and so-on. I have a top level wrapper with halo model code. This led to my question to how to add this to the data block. Its probably of great utility to have a likelihood DLS module. I’ve talked with the DLS team. I’ve got excited about sharing that around seeing how useful the other likelihood modules are.

Joe: Great! Anything we can do to help?

Michael: Getting these power spectra so I’m not doing things in a stupid way and so things are usable by other people as well for joint probes analyses. There can be information stored in the likelihoods. I couldn’t find information about standardised likelihoods. I want to do some diagnostic plots. If I run the test sampler. That seemed like a nice feature to have in the post processor module. I could use some help in making that a neat feature. I think useful to have that generically available to have the option of plotting data on theory. 

Joe: Yes, this is a great idea. Not always generic e.g. Planck. Not clear whether best to do in postprocessing or in the likelihood calculation.

Michael: Is there a place to marginalise nuisance parameters e.g. shear calibration. For n(z) is the approach to make a new n(z) and overwrite the old one?

Joe: Yes. That’s the way we’ve been thinking about it. 

Michael: That’s fine. I wondered about having a new module entirely as a cleaner style. 

Joe: I think if you can replace the output it can work quite well. We did this for sigma8. 

## Ideas for new modules ##

Joe: We discussed this earlier a bit already. One is likelihood modules, the other is samplers, infrastructure. Anything missing in terms of science modules?

Jason: The ISW-Galaxy cross correlations. Tommasso Giannantonio. Shirley Ho. Very useful in constraining MG parameters. For general cosmology its not a big thing but for general cosmology it can be very useful. 

Michael: Cosmology dependent covariance matrices - longer term.

Joe: Any public code doing this?

Michael: For DLS we calculated Gaussian terms and then multiplied that by the Takahashi. So there was a cosmology dependent term. 

## Ideas for other development ##

Renee: While I was messing around with the code I would do something very obviously stupid e.g. specify the wrong parameters. Then the error message was quite specific. I could do with a more clear message e.g. with the consistency relations the error messages are very clear. Some places where a flag would be more useful. Offline I could send you some examples.

Jason: Do you have a place to report this?

Joe: Yes, there is an issues page on the repository wiki page. People often think of issues as big things. But small things are very very good too.

Doug: I work with another collaboration and they find quite useful to upload information to hbin. Then someone submits that url to the collaboration and say here is the supporting information for my issue. 

Joe: Sounds really useful. Do we need to write our code or can we use that? Can it open an issue?

Doug: Just use theirs.

Michael: How much does that trade a headache for the user with a headache for Joe? 

Joe: Any issues?

Mustapha: I was going through the CosmoSIS paper and I came across a small section. Perhaps food for thought. It says we lose efficiency when we use a fully modular code. My point of view is that modular code is extremely important, even if we lose a little bit of efficiency. … Is there a plan to speed things up if they get too slow?

Joe: We don’t have any specific answers. Hard to run exactly the same code in two codes?

Scott: Why can’t you just compare?

Joe: You can. There are more things different between the codes than just one is modular. 

Mustapha: May be a goal 4-5 years from now is to have some strategies for optimising things. One could put a timing. What matters is the scientific results. Once the data is given and we want to find some scientific results. Maybe a comparison can be done on a long term.

Joe: Thank you very much indeed. Join the mailing lists