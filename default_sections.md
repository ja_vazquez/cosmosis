All of these sections are predefined strings that you can use in CosmoSIS modules.  They are typically used as the names of "sections" - groups of parameters and data collected together.

For an entry on this list called "name", in code you would use these pre-defined constants for the strings:

Python: cosmosis.names.name

C: NAME_SECTION

C++: NAME_SECTION

Fortran: name_section


#Likelihoods
    likelihoods

# Input parameters
    cosmological_parameters
    halo_model_parameters
    intrinsic_alignment_parameters
    baryon_parameters
    shear_calibration_parameters
    number_density_params

# Source number density
    wl_number_density

# 2-pt and related quantities
    matter_power_nl
    matter_power_lin
    shear_xi
    shear_cl
    galaxy_cl
    cmb_cl
    lss_autocorrelation
    gal_matter_power_lin
    linear_cdm_transfer
    sigma_r_lin
    sigma_r_nl

# Nuisance parameters
    planck
    intrinsic_alignment_field
    shear_calibration
    supernova_params

# Galaxy bias
    bias_field

# Background quantities
    distances
    de_equation_of_state

# Section for test likelihoods
    test_parameters
