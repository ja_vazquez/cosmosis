## Samplers ##

CosmoSIS comes with a range of samplers suitable for different likelihoods spaces.

Simple:

 - [test sampler](samplers/test) Evaluate a single parameter set
 - [list sampler](samplers/list) Re-run existing chain samples

 Classic:

 - [metropolis sampler](samplers/metropolis) Classic Metropolis-Hastings sampling
 - [importance sampler](samplers/importance) Importance sampling
 - [fisher sampler](samplers/fisher) Fisher Matrices

 Max-Like:

 - [maxlike sampler](samplers/maxlike) Find the maximum likelihood using various methods in scipy
 - [gridmax sampler](samplers/gridmax) Naive grid maximum-posterior

Ensemble:

 - [emcee sampler](samplers/emcee) Ensemble walker sampling
 - [kombine sampler](samplers/kombine) Clustered KDE
 - [multinest sampler](samplers/multinest) Nested sampling
 - [pmc sampler](samplers/pmc) Adaptive Importance Sampling
 
 Grid:

 - [grid sampler](samplers/grid) Regular posterior grid 
 - [snake sampler](samplers/snake) Intelligent Grid exploration
