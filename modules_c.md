# Writing a module in C #

```
#!C

#include "cosmosis/datablock/c_datablock.h"
#include "cosmosis/datablock/section_names.h"
#include "my_calculation_code.h"

const char * cosmo = COSMOLOGICAL_PARAMETERS_SECTION;
const char * like  = LIKELIHOODS_SECTION;

void * setup(c_datablock * options)
{
	// Read options from the CosmoSIS configuration ini file,
	// passed via the "options" argument

	// Set any global variables required

	// Record any configuration information required

	// Pass back any object you like
         int status = 0;
         status |= c_datablock_get_double(options, OPTION_SECTION, "mode", &mode);

         if (status){
                fprintf(stderr, "Please specify a mode in the CosmoSIS configuration ini file.\n");
                exit(status);
         }


}

int execute(c_datablock * block, void * config)
{
	
	// Config is whatever you returned from setup above
	// Block is the collection of parameters and calculations for
	// this set of cosmological parameters
        int status = 0;
        double w,omega_m;
        status |= c_datablock_get_double(block, cosmo, "w", &w);
        status |= c_datablock_get_double(block, cosmo, "omega_m", &omega_m);



        result = my_function(omega_m,w);
        //save to datablock
        status |= c_datablock_put_double(block, like, "MY_FUNCTION_LIKE", result);

	return status;
}


int cleanup(void * config)
{
	// Config is whatever you returned from setup above
	// Free it 

	return 0;
}

```

DESCRIBE WHAT ALL THIS MEANS.

## Listing of cosmosis C functions for modules ##

## GET scalar functions

You use these functions to load scalar values from the cosmosis datablock:

```
#!C
  DATABLOCK_STATUS
  c_datablock_get_int(c_datablock* s, const char* section, const char* name, int* val);

  DATABLOCK_STATUS
  c_datablock_get_bool(c_datablock* s, const char* section, const char* name, bool* val);

  DATABLOCK_STATUS
  c_datablock_get_double(c_datablock* s, const char* section, const char* name, double* val);

  DATABLOCK_STATUS
  c_datablock_get_complex(c_datablock* s, const char* section, const char* name, double _Complex* val);

  DATABLOCK_STATUS
  c_datablock_get_string(c_datablock* s, const char* section, const char* name, char** val);

```

You can also specify a default to be used if the value is not found:

```
#!C

  DATABLOCK_STATUS
  c_datablock_get_int_default(c_datablock* s, const char* section, const char* name,
			      int def, int* val);

  DATABLOCK_STATUS
  c_datablock_get_bool_default(c_datablock* s, const char* section, const char* name,
			       bool def, bool* val);

  DATABLOCK_STATUS
  c_datablock_get_double_default(c_datablock* s, const char* section, const char* name,
				 double def, double* val);

  DATABLOCK_STATUS
  c_datablock_get_string_default(c_datablock* s, const char* section, const char* name,
				 const char* def, char** val);

  DATABLOCK_STATUS
  c_datablock_get_complex_default(c_datablock* s, const char* section, const char* name,
				  double _Complex def,double _Complex* val);
```


## GET array functions

These functions load a 1D array of data from the block.  The val argument should not be allocated beforehand; cosmosis will allocate space for it.  You must ```free``` the array you get back after you have finished with it, for example:

```
#!C
double * x;
int nx;
int status = c_datablock_get_int_array_1d(block,"section", "name", &x, &nx);
// Do what you need to with x ...
free(x);
```

Full listing:

```
#!C

  DATABLOCK_STATUS
  c_datablock_get_int_array_1d(c_datablock* s, const char* section, const char* name,
			       int** val, int* size);

  DATABLOCK_STATUS
  c_datablock_get_double_array_1d(c_datablock* s, const char* section, const char* name,
				  double** val, int* size);

  DATABLOCK_STATUS
  c_datablock_get_complex_array_1d(c_datablock* s, const char* section, const char* name,
				   double _Complex** val, int* size);


```

If you have allocated an array or are using static arrays you can get data into this preallocated array. The maxsize integer tells cosmosis how much space you allocated for it; the returned size value tells you how much was actually used.  If maxsize is not large enough for the array you ask for then this is an error; a non-zero status is returned.

```
#!C
  DATABLOCK_STATUS
  c_datablock_get_int_array_1d_preallocated(c_datablock* s, const char* section, const char* name,
					    int* array, int* size, int maxsize);

  DATABLOCK_STATUS
  c_datablock_get_double_array_1d_preallocated(c_datablock* s, const char* section, const char* name,
					       double* array, int* size, int maxsize);

  DATABLOCK_STATUS
  c_datablock_get_complex_array_1d_preallocated(c_datablock* s, const char* section, const char* name,
						double _Complex* array, int* size, int maxsize);

```


## PUT scalar functions


You use these functions to save scalar values in the cosmosis datablock:


```
#!C

  DATABLOCK_STATUS
  c_datablock_put_int(c_datablock* s, const char* section, const char* name, int val);

  DATABLOCK_STATUS
  c_datablock_put_bool(c_datablock* s, const char* section, const char* name, bool val);

  DATABLOCK_STATUS
  c_datablock_put_double(c_datablock* s, const char* section, const char* name, double val);

  DATABLOCK_STATUS
  c_datablock_put_complex(c_datablock* s, const char* section, const char* name, double _Complex val);

  DATABLOCK_STATUS
  c_datablock_put_string(c_datablock* s, const char* section, const char* name, const char* val);

```


#PUT arrays functions

These functions save 1D arrays to the cosmosis datablock.

``` 
#!C

  DATABLOCK_STATUS
  c_datablock_put_int_array_1d(c_datablock* s, const char* section, const char* name,
			       int const*  val, int sz);

  DATABLOCK_STATUS
  c_datablock_put_double_array_1d(c_datablock* s, const char* section, const char* name,
				  double const*  val, int sz);

  DATABLOCK_STATUS
  c_datablock_put_complex_array_1d(c_datablock* s, const char* section, const char* name,
				   double _Complex const*  val, int sz);


```

## REPLACE scalar functions

You use these functions to replace existing scalar values in the cosmosis datablock:

```
#!C
  DATABLOCK_STATUS
  c_datablock_replace_int(c_datablock* s, const char* section, const char* name, int val);

  DATABLOCK_STATUS
  c_datablock_replace_bool(c_datablock* s, const char* section, const char* name, bool val);

  DATABLOCK_STATUS
  c_datablock_replace_double(c_datablock* s, const char* section, const char* name, double val);

  DATABLOCK_STATUS
  c_datablock_replace_complex(c_datablock* s, const char* section, const char* name, double _Complex val);

  DATABLOCK_STATUS
  c_datablock_replace_string(c_datablock* s, const char* section, const char* name, const char* val);

```


## REPLACE array functions

These functions replace 1D array data in the cosmosis data block.

```
#!C
  DATABLOCK_STATUS
  c_datablock_replace_int_array_1d(c_datablock* s, const char* section, const char* name,
				   int const* val, int sz);

  DATABLOCK_STATUS
  c_datablock_replace_double_array_1d(c_datablock* s, const char* section, const char* name,
				      double const* val, int sz);

  DATABLOCK_STATUS
  c_datablock_replace_complex_array_1d(c_datablock* s, const char* section, const char* name,
				       double _Complex const* val, int sz);
```


TODO: Document n-dimensional array functions