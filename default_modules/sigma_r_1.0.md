
# sigma_r module

## Compute anisotropy dispersion sigma(R,z)

**Name**: sigma_r

**File**: cosmosis-standard-library/boltzmann/sigmar/sigmar.py

**Version**: 1.0

**Author(s)**:

- Scott Dodelson

**URL**: 

**Cite**: 



**Rules**: 

- 

**Assumptions**:

- minimal assumptions; sigma computed directly from P(k,z)

**Explanation**

sigma(R,z) is the variance of cosmic density fluctuations on scales down to 8 Mpc/h.

In Fourier space is given by sigma(R,z) = \int_0^\infty W^2(k R) k^2 P(k,z) / (2 \pi)^2 dk

The P(k,z) used could in general be linear or non-linear, but usually when people say sigma they mean the non-linear variant. 


##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
zmin|real, minimum redshift to generate values for
zmax|real, maximum redshift to generate values for
dz|real, output redshift sample spacing
rmin|real, minimum scale R in Mpc/h to generate values for
rmax|real, maximum scale R in Mpc/h to generate values for
dr|real, scale R spacing
matter_power|name of section to get P(k,z) from, e.g. matter_power_lin, matter_power_nl

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
matter_power|k_h|real 1D array, sample values of linear spectrum in Mpc/h.  Section name specified by parameter in ini file.
|z|real 1D array, redshift of linear spectrum samples.  Section name specified by parameter in ini file.
|P_k|real 2D array, linear spectrum in (Mpc/h)^{-3}.  Section name specified by parameter in ini file.

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
sigmar|R|real 1D array, scale R of output in Mpc/h
|z|real 1D array, redshift of output
|sigma2|real 2D array, variance sigma^2(R,z)


