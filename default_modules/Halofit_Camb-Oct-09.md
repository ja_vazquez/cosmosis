
# Halofit module

## Compute non-linear matter power spectrum

**Name**: Halofit

**File**: cosmosis-standard-library/boltzmann/halofit/halofit_module.so

**Version**: Camb-Oct-09

**Author(s)**:

- Antony Lewis
- Simeon Bird

**URL**: http://camb.info

**Cite**: 

- http://arxiv.org/abs/astro-ph/0207664
- http://arxiv.org/abs/1208.2701

**Rules**: 

- Please abide by the conditions set out in the CAMB license if you use this module http://camb.info/CAMBsubmit.html

**Assumptions**:

- Fitting formula from Takahashi et al applies only to LCDM

**Explanation**

Halofit is a fitting function based on simulations which models the non-linear matter-power spectrum by scaling the linear one.


##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
kmin|real, minimum k in Mpc/h to generate values for (default 1e-4)
kmax|real, maximum k in Mpc/h to generate values for (default 100.0
nk|integer, number of log-spaced k values to sample (default 200)

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
cosmological_parameters|omega_m|real, Total matter density fraction today
|omega_lambda|real, Dark energy density fraction today
|omega_nu|real, Neutrino density fraction today (default 0.0)
|w|real, Dark energy equation of state (default -1)
matter_power_lin|k_h|real 1D array, sample values of linear spectrum in Mpc/h
|z|real 1D array, redshift of linear spectrum samples
|P_k|real 2D array, linear spectrum in (Mpc/h)^{-3}

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
matter_power_nl|k_h|real 1D array, sample values of nonlinear spectrum in Mpc/h
|z|real 1D array, redshift of nonlinear spectrum samples
|P_k|real 2D array, nonlinear spectrum in (Mpc/h)^{-3}


