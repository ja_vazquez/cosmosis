
# wmap_shift module

## Massively simplified WMAP9 likelihood reduced to just shift parameter

**Name**: wmap_shift

**File**: cosmosis-standard-library/likelihood/wmap_shift/wmap_shift.so

**Version**: 1.0

**Author(s)**:

- The WMAP Collaboration (measurement)
- CosmoSIS team (code)

**URL**: 

**Cite**: 

- Hinshaw et al, ApJS, 208, 2, 19, 25

**Rules**: 

- 

**Assumptions**:

- CMB shift parameter as in LCDM
- WMAP9 measurement of parameter

**Explanation**

The full WMAP likelihood is slow and requires a full Boltzmann integration (also slow) to get the CMB spectra.

This module uses a lightweight alternative - the CMB shift parameter, which can be calculated from background evolution alone.

This does not provide as much information as the full likelihood. 


##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------


##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
distances|cmbshift|CMB Shift parameter

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
likelihoods|shift_like|Combined log-likelihood from all WMAP components


