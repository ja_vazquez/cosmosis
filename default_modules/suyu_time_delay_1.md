
# suyu_time_delay module

## 

**Name**: suyu_time_delay

**File**: cosmosis-standard-library/strong_lensing/suyu_time_delay/suyu_interface.py

**Version**: 1

**Author(s)**:

- Suyu et al data

**URL**: 

**Cite**: 

- http://arxiv.org/pdf/1306.4732v2.pdf and http://arxiv.org/pdf/0910.2773v2.pdf

**Rules**: 

- 

**Assumptions**:

- Strong lensing modelling details.

**Explanation**

 The likelihood of a strong lensing time-delay system as modelled in http://arxiv.org/pdf/1306.4732v2.pdf and http://arxiv.org/pdf/0910.2773v2.pdf



 


##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
lens_name|String, default='None'; name of lens to use. B1608 and RXJ1131 are accepted, if 'None', user must set remaining parameter manually
z_d|Real, only if lens_name='None'. Distance to the lens
z_s|Real, only if lens_name='None'. Distance to the source
lambda_d|Real, only if lens_name='None'. See 0910.2773v2 equation 5
mu_d|Real, only if lens_name='None'. See 0910.2773v2 equation 5
sigma_d|Real, only if lens_name='None'. See 0910.2773v2 equation 5
name|String, Name for the strong lens

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
distances|z|1D real array, redshifts of samples
|d_m|1D real array, co-moving distance in Mpc
cosmological_parameters|omega_k|real, curvature density fraction today (default 0.0)
|hubble|real, hubble parameter H0 (km/s/Mpc)

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
likelihoods|B1608_like|Real, only if lens_name=B1608. Likelihood of B1608 system
|RXJ1131_like|Real, only if lens_name=RXJ1131. Likelihood of RXJ1131 system
|name_like|Real, general case, name from ini file. Likelihood of named system


