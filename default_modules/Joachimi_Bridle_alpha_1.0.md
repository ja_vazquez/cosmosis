
# Joachimi_Bridle_alpha module

## Calculate the gradient of the galaxy luminosity function at the limiting magnitude of the survey.

**Name**: Joachimi_Bridle_alpha

**File**: cosmosis-standard-library/luminosity_function/Joachimi_Bridle_alpha/interface.py

**Version**: 1.0

**Author(s)**:

- CosmoSIS team
- Simon Samuroff

**URL**: 

**Cite**: 



**Rules**: 

- 

**Assumptions**:

- The galaxy luminosity function is well approximated by the fitting function of Blake & Bridle (2005).
- The limiting r-band magnitude r_lim>16.9 

**Explanation**

 The gradient of the cumulative galaxy luminosity function \alpha is sensitive to both redshift and magnitude limit of the survey  considered. Joachimi & Bridle (2010) extend the fitting function of Blake & Bridle (2005) to obtain a polynomial \alpha(z, r_lim)  at a range of redshifts, where z_i is the median in redshift bin i and r_lim is the r-band magnitude limit. Note that the fitting is based on ground-based data from the COMBO-17 survey. See Joachimi & Bridle (2010) for discussion of its applicability.  


##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
magnitude_limit|double; limiting r-band magnitude of the survey considered. default=24
binned_alpha|bool; compute alpha in the survey redshift bins, rather than as a continuous funtion of redshift. default=True

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
wl_num_density|Nz|integer; number of points used to evaluate the distribution in each redshift bin.
|nzbin|integer; number of survey redshift bins.
|zmax|double; maximum redshift of the redshift distributions.
|bin_i|real vector; an array of Nz points evenly sampled from the galaxy redshift distribution in bin i in the range z={0...zmax}. The index ranges i={0,1...nzbin}.

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
galaxy_luminosity_function|z|real vector; redshift values of alpha(z) samples
|alpha|real vector; gradient of the logarithmic cumulative galaxy luminosity function at the limiting magnitude.
|z_binned|real vector; median values of the n(z) in the survey redshift bins.
|alpha_binned|real vector; gradient of the logarithmic cumulative galaxy luminosity function at the limiting magnitude, evaluated at the median redshift of each bin.


