
# cl_to_xi module

## Compute WL correlation functions xi+, xi- from C_ell

**Name**: cl_to_xi

**Directory**: cosmosis-standard-library/shear/cl_to_xi

**Version**: 1.0

**Author(s)**:

- CosmoSIS team
- Niall MacCrann

**URL**: 

**Cite**: 



**Rules**: 

- 

**Assumptions**:

- To lessen ringing we extrapolate log-linearly beyond k_max in P(k)
- Some ringing still remains in the outputs of this code; it will not be suitable for high-res calculations

**Explanation**

 The correlation functions are related to the spectra via Bessel functions: xi_{(+/-)}(theta) = \int_0^\infty C_\ell J_{(0,4)}(\ell \theta) \ell d\ell / 2\pi

In this module that integral is done directly via trapezium rule summation of sample values.

Because we do not really have C_\infty there are edge effects at low \theta from the cut-off which can cause ringing and similar phenomena.  We try to  minimize those by extrapolation C_\ell, but some will remain.

Better solutions very welcome! 


##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
n_theta|Integer; number of log-spaced theta values to compute
theta_min|Real; minimum theta value to compute in arcmin
theta_max|Real; maximum theta value to compute in arcmin
theta|Real vector; can specify this instead of the three above to choose theta explicitly

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
shear_cl|ell|Sample ell values for output C_ell
|nbin|Number of redshift bins used
|bin_i_j|C_ell (no l(l+1) factor) for (auto-correlation) bin i and j. Only need j<=i.

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
shear_xi|theta|Sample theta values for output xi(theta)
|xiplus_i_j|xi_plus(theta) (auto-correlation) bin i and j. Only stores j<=i.
|ximinus_i_j|xi_minus(theta) (auto-correlation) bin i and j. Only stores j<=i.


