
# smail module

## Compute window functions for photometric n(z)

**Name**: smail

**File**: cosmosis-standard-library/number_density/smail/photometric_smail.py

**Version**: 1

**Author(s)**:

- CosmoSIS Team

**URL**: 

**Cite**: 



**Rules**: 

- You can do what you want with this file

**Assumptions**:

- Underlying true number density has Smail distribution
- Photometric errors are sigma(z) = sigma_0 (1+z)
- Bias fixed with redshift (if included)
- Galaxies evenly divided between bins

**Explanation**

This module takes inputs that specify the underlying spectroscopic (true) redshift distribution of the galaxies in the survey.  It then convolves this with a photometric error sigma(z) = sigma_0 (1+z) and optionally biases it.  It computes bin edges in the survey assuming equal numbers in each.

We might wish to add an option to specify fixed bin edges instead? 


##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
nbin|Integer; Number of redshift bins with equal number of gals in eachq
zmax|Double; Maximum redshift to compute; min is zero
dz|Double; spacing of samples to compute n(z) at.

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
number_density_params|alpha|Double; Smail distribution parameter. n(z) = z^{alpha} exp{-(z/z0)^beta}
|beta|Double; Smail distribution parameter
|z0|Double; Smail distribution parameter
|sigz|Double; Photometric error at z=0
|ngal|Double; Total number density of galaxies per square arcmin
|bias|Double; Bias on all photometric measurements

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
wl_number_density|nz|Integer; number of redshift samples
|nbin|Integer; number of bins
|z|Real vector; redshift sample values
|bin_|Real vector; n(z) at redshift sample values.  bin_1, bin_2, ...
|edge_|Real vector;  The nominal edges of the redshift bins (i.e. edges if no photometric errors)


