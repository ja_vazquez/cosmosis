
# photoz_bias module

## Modify a set of loaded n(z) distributions with a multiplicative or additive bias

**Name**: photoz_bias

**File**: cosmosis-standard-library/number_density/photoz_bias/photoz_bias.py

**Version**: 1

**Author(s)**:

- CosmoSIS Team

**URL**: 

**Cite**: 



**Rules**: 

- 

**Assumptions**:

- Simple photo-z bias models: n(z) -> n(z-b) or n(z*(1-b))

**Explanation**

Photometric redshift distributions can contain biases - the actual distribution of galaxies in a survey can be different to the estimated one.

This bias can remain even after calibration with a spectroscopic sample, or by other methods.

This module models the simplest possible type of n(z) bias - a simple shift in z by a multiplicative or additive factor. The idea is that the shift parameter should be marginalized in sampling over to  account for this bias.  Note that this is *not* the same as simply widening the n(z).

Ranges or priors should be put on the size of the bias that reflect your knowledge of remaining possible biases.

The mode is: n(z) -> n(z-b) or n(z*(1-b))


##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
mode|String; 'multiplicative' or 'additive', depending on what kind of bias model you want

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
wl_number_density|nbin|Integer; number of redshift bins
|z|Real vector; redshift sample points of n(z) estimates
|bin_i|Real vectors; for i=1..nbin. n(z) estimates
wl_photoz_errors|bias_i|Real; for i=1..nbin. Bias size for this bin.

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
wl_number_density|bin_i|Real vectors; for i=1..nbin. Modified n(z) estimates


