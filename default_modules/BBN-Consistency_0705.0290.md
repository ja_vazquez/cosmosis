
# BBN-Consistency module

## Compute consistent Helium fraction from baryon density given BBN

**Name**: BBN-Consistency

**File**: cosmosis-standard-library/utility/bbn_consistency/bbn_consistency.py

**Version**: 0705.0290

**Author(s)**:

- CosmoSIS Team

**URL**: http://parthenope.na.infn.it/

**Cite**: 

- Comp.Phys.Commun.178:956-971,2008

**Rules**: 



**Assumptions**:

- Standard Big Bang Nucleosynthesis

**Explanation**

 The Big Bang Nucleosynthesis model describes how the  light elements were generated in the primordial universe.  For a given value of Omega_b h**2 and number of neutrinos the theory can predict the helium abundance.

This module sets the helium mass fraction (YHe) from the mean baryon density (ombh2) and number of neutrinos (delta_neff), based on a table interpolation from those calculations.

This module should go into the pipeline after consistency and before any physics modules. It's effectively an optional consistency module.




##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
data|string. Filename for ombh2,DeltaN,YHe data. (default=included file)

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
cosmological_parameters|ombh2|real, physical matter density parameter
|delta_neff|real, optional. Extra contribution to neutrino number density

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
cosmological_parameters|yhe|real, cosmological helium fraction


