
# jla module

## Supernova likelihood for SDSS-II/SNLS3

**Name**: jla

**File**: cosmosis-standard-library/supernovae/jla_v3/jla.so

**Version**: 3

**Author(s)**:

- Marc Betoule

**URL**: http://supernovae.in2p3.fr/sdss_snls_jla/ReadMe.html

**Cite**: 

- http://arxiv.org/abs/1401.4064

**Rules**: 

- 

**Assumptions**:

- SALT2 used to fit light curves
- Akima interpolation between mu(z) samples

**Explanation**

This JLA code uses 731 supernovae from the JLA SDSS-II/SNLS3 sample  to get a likelihood of a given theory mu(z).

Systematic error propagation is done with a collection of separate  covariance matrices for the various light-curve parameters.

You can copy the standard parameters to use for this from demos/demo5.ini 


##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
data_dir|String; dir for other files. Use cosmosis-standard-library/supernovae/jla_v3/data (unless trying a different data set)
data_file|String; dir for other files. Use jla_lcparams.txt (unless trying a different data set)
scriptmcut|String; dir for other files. Use 10.0
mag_covmat_file|String; dir for other files. Use jla_v0_covmatrix.dat
stretch_covmat_file|String; dir for other files. Use jla_va_covmatrix.dat
colour_covmat_file|String; dir for other files. Use jla_vb_covmatrix.dat
mag_stretch_covmat_file|String; dir for other files. Use jla_v0a_covmatrix.dat
mag_colour_covmat_file|String; dir for other files. Use jla_v0b_covmatrix.dat
stretch_colour_covmat_file|String; dir for other files. Use jla_vab_covmatrix.dat

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
distances|z|Real vector; redshift sample values for theory 
|mu|Real vector; theory distance modulus at sample redshifts
supernova_params|alpha|Real; SN shape parameter coefficient
|beta|Real; SN color parameter coefficient
|M|Real; SN magnitude parameter baseline value; leave this fixed and vary deltaM.
|deltaM|Real; SN magnitude parameter where M_actual = M + deltaM

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
likelihoods|JLA_LIKE|Gaussian likelihood for this data set and theory mu(z)


