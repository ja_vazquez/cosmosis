
# linear_alignments module

## Compute the terms P_II and P_GI which go into intrinsic aligment calculations

**Name**: linear_alignments

**File**: cosmosis-standard-library/intrinsic_alignments/la_model/linear_alignments_interface.py

**Version**: 1.0

**Author(s)**:

- CosmoSIS team
- Donnacha Kirk

**URL**: 

**Cite**: 

- MNRAS 424 3 1647 (2012)
- New J Phys 9 12 444 (2007)

**Rules**: 

- 

**Assumptions**:

- Uses one of three models for how matter power is turned into intrinsic alignments

**Explanation**

 Intrinsic alignment refers to the extent to which galaxies align in the sky, before any alignment induced by gravitational lensing.  It is a systematic error contribution to cosmic shear measurements and is predicted here in the form of two power spectra, one for the alignments, P_II, and one for the alignment-shear correlations, P_GI.

In the original Linear Alignment model it was assumed that alignments among galaxies were laid down early in the evolution of structure, and then (on average) did not vary since.  The amount of alignment on a given scale was then related to the linear power spectrum at that scale.

The P_II and P_GI power is described in terms of a power spectrum, integrated over with a Limber integral in a similar way to the shear power spectrum. This code does not do that integral, it just calculates P_II and P_GI.

A number of variations to the LA model have been discussed since, and this module implements three of them.  The history is a little convoluted as an error was found in early work missing a factor of (1+z), so one of our models is a corrected version of one of the other ones.  Our models are:

    Bridle & King

    Bridle & King (corrected)

    Kirk, Rassat, Host, Bridle



See these papers for details of these models.




##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
method|string, choice of 'bk', 'bk_corrected', 'krhb', chooses which model to run

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
cosmological_parameters|omega_m|Real; density fraction of all matter.
intrinsic_alignment_parameters|A|real; single parameter scaling power spectra
matter_power_nl|z|real vector; redshift values of P(k,z) samples
|k_h|real vector; k values of P(k,z) samples in units of Mpc/h
|P_k|real 2d array; non-linear matter power spectrum at samples in (Mpc/h)^{-3}
matter_power_lin|z|real vector; redshift values of P(k,z) samples
|k_h|real vector; k values of P(k,z) samples in units of Mpc/h
|P_k|real 2d array; nonlinear matter power spectrum at samples in (Mpc/h)^{-3}

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
intrinsic_alignment_parameters|z|real vector; redshift values of P(k,z) samples
|k_h|real vector; k values of P(k,z) samples in units of Mpc/h
|P_II|real 2d array; spectrum of intrinsic-intrinsic power at samples in (Mpc/h)^{-3}
|P_GI|real 2d array; spectrum of shear-intrinsic power at samples in (Mpc/h)^{-3}


