
# Tinker_MF module

## Code to compute the Tinker et al. mass function given Pk from CAMB, based on Komatsu's CRL

**Name**: Tinker_MF

**File**: cosmosis-standard-library/mass_function/mf_tinker/tinker_mf_module.so

**Version**: 1

**Author(s)**:



**URL**: http://www.mpa-garching.mpg.de/~komatsu/crl/

**Cite**: 

- http://www.mpa-garching.mpg.de/~komatsu/crl/
- Tinker et al 2008

**Rules**: 

- 

**Assumptions**:

- Tinker mass function
- Require P(k,z) from CAMB or otherwise

**Explanation**

This module calculates the Tinker et al. mass function given the linear matter power spectrum.


##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
feedback|Integer,amount of output to print.  0 for no feedback.  1 for basic (default = 0)
redshift_0|Integer. 1 outputs only z=0 mf. 0 outputs mass functions for each Pk in datablock

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
matter_power_lin|k|real 1D array, sample values of linear spectrum in Mpc/h
|z|real 1D array, redshift of linear spectrum samples
|P|real 2D array, linear spectrum in (Mpc/h)^{-3} 

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
mass_function|r_h|real 1D array, R in (h^-1 Mpc) 
|m_h|real 1D array, mass in (omega_matter h^-1 M_solar)
|dndlnrh|real 2D array,  dn/dlnRh (h^3 Mpc^-3)
|dndlnmh|real 2D array, dn/dlnMh (h^3 Mpc^-3) 


