
# gaussian_window module

## Compute Gaussian n(z) window functions for weak lensing bins

**Name**: gaussian_window

**File**: cosmosis-standard-library/number_density/gaussian_window/gaussian_window.py

**Version**: 1

**Author(s)**:

- CosmoSIS Team

**URL**: 

**Cite**: 



**Rules**: 

- You can do what you want with this file

**Assumptions**:

- Gaussian window in redshift

**Explanation**

This very simple module sets up fixed redshift n(z) bins for weak lensing.

 We should probably upgrade it to take the redshift and sigma from the 

 sampling instead of the ini file.


##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
z|Real scalar or vector; redshift(s) of the bins
sigma|Real scalar or vector; width of the bins in redshift

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------


##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
wl_number_density|nz|Integer; number of redshift samples
|nbin|Integer; number of bins
|z|Real vector; redshift sample values
|bin_|Real vector; n(z) at redshift sample values.  bin_1, bin_2, ...


