
# balmes module

## 

**Name**: balmes

**File**: cosmosis-standard-library/strong_lensing/balmes_corasaniti/balmes.py

**Version**: 1

**Author(s)**:

- I. Balmes & P.S. Corasaniti

**URL**: 

**Cite**: 

- arXiv:1206.5801 

**Rules**: 

- Please cite the relevant papers if you use this module.

**Assumptions**:

- Strong lensing modelling details

**Explanation**

 Balmes & Corasaniti measured H0 using strong lensing systems.

This module uses a likelihood tabulated from their paper.

 


##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
data_file|String; dir for data files. Data file containing 2 columns H0 and P. You should use the file taken from arXiv:1206.5801 and provided in CosmoSIS under the name balmes.txt unless you want to use a different dataset 

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
cosmological_parameters|h0|Real, Hubble parameter/100 km/s/Mpc 

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
likelihoods|balmes_sl_like|1D real, likelihood of this strong lensing system given h0


