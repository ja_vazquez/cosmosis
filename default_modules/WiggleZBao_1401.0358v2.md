
# WiggleZBao module

## Compute the likelihood of the supplied expansion history against WiggleZ BAO data

**Name**: WiggleZBao

**File**: cosmosis-standard-library/likelihood/wigglez_bao/wigglez_bao.py

**Version**: 1401.0358v2

**Author(s)**:

- WiggleZ Team
- MontePython Team

**URL**: http://www.smp.uq.edu.au/wigglez-data/

**Cite**: 

- MNRAS 441, 3524 (2014)

**Rules**: 

- 

**Assumptions**:

- WiggleZ dark energy survey data set
- FLRW metric and standard BAO size

**Explanation**

This module gives a likelihood of the redshift-distance and redshift-Hubble relations in combined form D_v = (da**2 * (1+z)**2 * dr)**(1./3.)  where dr = z / H. It uses the sound horizon at last-scatter rs_zdrag and  the predicted expansion since last scattering to predict the BAO size at the redshifts at which the WiggleZ survey measured them.

A correlated Gaussian likelihood is then returned.


##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
data_file|string, path to file with measured z - D_v values in (default: included file)
weight_file|string, path to inverse covariance matrix file (default: included file)
rs_fiducial|Real, Fiducial value of sound horizon at last scattering used in making data (default=148.6)
verbose|Bool, Print extra output (default = False)

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
distances|z|1D real array, redshifts of samples
|d_a|1D real array, angular diameter distance in Mpc
|h|1D real array, hubble parameter with in units of Mpc
|rz_zdrag|real, sound horizon at last scattering in Mpc

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
likelihoods|wigglez_bao_like|real, likelihood of supplied expansion history


