
# FrankenEmu module

## Emulate N-body simulations to compute nonlinear matter power

**Name**: FrankenEmu

**File**: cosmosis-standard-library/structure/FrankenEmu/interface.so

**Version**: 2.0

**Author(s)**:

- Suman Bhattacharya
- Salman Habib
- Katrin Heitmann
- David Higdon
- Juliana Kwan
- Earl Lawrence
- Christian Wagner
- Brian Williams
- Martin White

**URL**: http://www.hep.anl.gov/cosmology/CosmicEmu/emu.html

**Cite**: 

- The Coyote Universe Extended, arXiv:1304.7849
- Coyote Universe I: ApJ 715, 104 (2010), arXiv:0812.1052
- Coyote Universe II: ApJ 705, 156 (2009), arXiv:0902.0429
- Coyote Universe III: ApJ 713, 1322 (2010), arXiv:0912.4490

**Rules**: 

- 

**Assumptions**:

- LCDM in the form of the Coyote Universe simulations

**Explanation**

 FrankenEmu is an emulator designed to interpolate among a collection of numerical N-body simulations called the Coyote Universe.

It uses a Gaussian Process interpolation between a set of simulations arranged in a Latin Hypercube in parameter space.

Each simulation yields a non-linear matter power spectrum P(k,z), and the interpolation is between these spectra, so the output should be a reasonable (1% accuracy) value of P(k,z) for the given parameters.

The simulations and the whole process are explained in detail in the papers above. 


##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
dz|Spacing in redshift of output
nz|Number of redshift samples.  Need nz*dz<=4.0
do_distances|boolean, Whether to also calculate cosmological distances (default=T)

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
cosmological_parameters|ombh2|real scalar, baryon content
|ommh2|real scalar, matter content
|w|real scalar, dark energy EoS.  Optional; default=-1
|h0|real scalar, Hubble/100km/s/Mpc
|n_s|real scalar, scalar spectral index
|sigma_8|real scalar, scalar spectral index running. Options; default=-1

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
matter_power_nl|z|1D real array, redshifts of samples
|k_h|1D real array, k wavenumbers of samples in Mpc/h
|p_k|2D real array, matter power spectrum at samples in (Mpc/h)^-3
distances|z|1D real array, redshifts of samples
|a|1D real array, scale factor of samples
|d_a|1D real array, angular diameter distance in Mpc
|d_m|1D real array, co-moving distance in Mpc
|h|1D real array, hubble parameter with in units of c/Mpc


