
# Cluster_mass module

## Likelihood of z=1.59 Cluster mass from Santos et al. 2011

**Name**: Cluster_mass

**File**: cosmosis-standard-library/likelihood/cluster_mass/cluster_mass.py

**Version**: 1.0

**Author(s)**:

- Santos et al. 2011 (measurement)
- Harrison & Coles 2012
- CosmoSIS team (code)

**URL**: 

**Cite**: 

- Santos et al. 2011 
- Harrison & Coles 2012 

**Rules**: 



**Assumptions**:



**Explanation**

This small module was written for CosmoSIS.

    The Extreme Value statistics module (evs) should be run in the pipeline prior to this module.




##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
mean|float, mass in M_sun/h
sigma|float, error in M_sun/h 

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
clusters|M_max|real, mass (M_sun/h)

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
likelihoods|MAXMASS_LIKE|Gaussian likelihood value of supplied parameters


