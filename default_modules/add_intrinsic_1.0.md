
# add_intrinsic module

## Sum together intrinsic aligments with shear signal

**Name**: add_intrinsic

**File**: cosmosis-standard-library/shear/add_intrinsic/add_intrinsic.py

**Version**: 1.0

**Author(s)**:

- CosmoSIS team

**URL**: 

**Cite**: 



**Rules**: 

- 

**Assumptions**:

- Linear sum of C_ell for IA components

**Explanation**

Observerd shape spectra contain a sum of intrinsic and shear spectra, and the cross-correlation between them.  This module adds together these components, accounting for the fact that C_GI != C_IG for two bins

It may be replaced at some point with changes to the shear computation modules.


##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------


##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
shear_cl_gg|nbin|Integer; number of tomographic bins
|ell|Real 1D array; ell samples of theory
|bin_{i}_{j}|Real 1D array; for series of i,j values, the shear-shear angular spectra C_ell
shear_cl_ii|bin_{i}_{j}|Real 1D array; for series of i,j values, the intrinsic-intrinsic angular spectra C_ell
shear_cl_gi|bin_{i}_{j}|Real 1D array; for series of i,j values, the shear-intrinsic angular spectra C_ell
|bin_{j}_{i}|Real 1D array; for series of i,j values, the intrinsic_shear angular spectra C_ell

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
shear_cl|nbin|Integer; number of tomographic bins
|ell|Real 1D array; ell samples of theory
|bin_{i}_{j}|Real 1D array; for series of i,j values, the total angular spectra C_ell


