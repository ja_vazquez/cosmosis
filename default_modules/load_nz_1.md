
# load_nz module

## Load a number density n(z) for weak lensing from a file

**Name**: load_nz

**File**: cosmosis-standard-library/number_density/load_nz/load_nz.py

**Version**: 1

**Author(s)**:

- CosmoSIS Team

**URL**: 

**Cite**: 



**Rules**: 

- If you use a file from a particular survey you should cite that survey

**Assumptions**:

- n(z) file first column = z, others = bin n(z)

**Explanation**

This simple module just loads a set of n(z) for different bins from a fixed file and provides it as-is.  The n(z) are normalized before being saved.


##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
filepath|String; absolute or relative path to an n(z) file
des_fmt|Bool, default=F; use the DES format n(z) with columns zmin, zmax, nz1, nz2...
histogram|Bool, default=F; Assume that the given z values are lower edges of histogram bins, not sample points.

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------


##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
wl_number_density|nz|Integer; number of redshift samples
|nbin|Integer; number of bins
|z|Real vector; redshift sample values
|bin_|Real vector; n(z) at redshift sample values.  bin_1, bin_2, ...


