
# BBN module

## Simple prior on Omega_b h^2 from light element abundances

**Name**: BBN

**File**: cosmosis-standard-library/likelihood/bbn/bbn_ombh2.py

**Version**: PDG13

**Author(s)**:

- B.D Fields
- P. Molaro
- S. Sarkar

**URL**: http://pdg.lbl.gov/2013/reviews/rpp2013-rev-bbang-nucleosynthesis.pdf

**Cite**: 

- J. Beringer et al. (Particle Data Group), Phys. Rev. D86, 010001 (2012)

**Rules**: 

- None.

**Assumptions**:

- Standard model of Big-Bang nucleosynthesis

**Explanation**

This small module was written for CosmoSIS.

Measurements of the abundances of light elements D, 3He, 4He, and 7Li constrain the density budget at the epoch of nucleosynthesis in the first three minutes after the big bang. 


##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
mean|float, replace the standard value measurement omega_b h^2 = 0.023 with a custom one (default 0.023)
sigma|float, replace the standard value error 0.002 with a custom one (default 0.002)

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
cosmological_parameters|omega_b|real, baryon density fraction today
|h0|real, hubble parameter H0/(100 km/s/Mpc)

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
likelihoods|BBN_LIKE|Gaussian likelihood value of supplied parameters


