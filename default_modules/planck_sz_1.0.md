
# planck_sz module

## Prior on sigma_8 * Omega_M ** 0.3 from Planck SZ cluster counts

**Name**: planck_sz

**File**: cosmosis-standard-library/likelihood/sz/sz.py

**Version**: 1.0

**Author(s)**:

- Planck collaboration (measurement)
- CosmoSIS team (code)

**URL**: 

**Cite**: 

- arXiv:1303.5080, Planck 2013 results. XX. Cosmology from Sunyaev-Zeldovich cluster counts

**Rules**: 

- None.

**Assumptions**:

- Planck SZ data
- SZ signal Y - Mass relation calibrated from X-ray data
- Flat LCDM
- Tinker et al halo distribution

**Explanation**

This small module was written for CosmoSIS.

CMB data like that from Planck can be used to make counts of clusters using the Sunyaev-Zeldovich effect, in which hot gas in the clusters scatters CMB photons and causes a decrement (below 217 GHz) or increment (above 217 GHz).

The number of clusters in the universe of a given mass is sensitive to the  mass density and the overall amount structure in the universe.  The full calculation is very involved, but in a LCDM universe with some assumptions about Halo behaviour it can be reduced to a constraint on sigma_8 * Omega_M ** 0.3




##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
measured_value|float, replace the standard value measurement sigma_8 * Omega_M ** 0.3 = 0.764 with a custom one for simulations (default 0.764)

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
cosmological_parameters|omega_m|real, matter density fraction at redshift 0
|sigma_8|real, matter fluctuation dispersion down to 8 Mpc/h

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
likelihoods|SZ_LIKE|Gaussian likelihood value of supplied parameters


