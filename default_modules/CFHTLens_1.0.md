
# CFHTLens module

## Compute the likelihood of CFHTLens tomographic data given input shear correlation functions

**Name**: CFHTLens

**File**: cosmosis-standard-library/likelihood/cfhtlens/cfhtlens_interface.py

**Version**: 1.0

**Author(s)**:

- CFHTLens Team (data)
- CosmoSIS Team (module code)

**URL**: http://www.cfhtlens.org/astronomers/content-suitable-astronomers

**Cite**: 

- Heymans et al, MNRAS, 432, 3, p.2433-2453

**Rules**: 

- See required acknowledgement text: http://www.cfhtlens.org/astronomers/data-store

**Assumptions**:

- Heymans et al 6 bin tomographic data set
- Redshift distribution used correctly matches tomographic bins

**Explanation**

CFHTLens measured the cosmic shear 2-pt correlation functions xi_+(theta)  and xi_minus(theta) on 154 square degrees of the sky in ugriz bands.

This module calculates the likelihood of theoretical xi(theta) values values given the CFHTLens measurements. 


##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
data_file|String. filename containing data in CFHTLens ordering.  Default is file supplied with module
covariance_file|String. covariance matrix of supplied data.  Default is file supplied with module
xiplus_only|Boolean.  Ignore xi_minus and use only xi_plus, which is less affected by small scale effects. Default False.
cut_low_theta|Boolean.  Remove the smallest scale measurements which are also affected more by baryons and NL power.  Default True

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
shear_xi|theta|1D real array, theta values of theory correlation functions
|xi_plus_i_j|xi_plus meausurements for i,j=(1..6)
|xi_minus_i_j|xi_minus meausurements for i,j=(1..6)

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
likelihoods|cfhtlens_like|real, likelihood of supplied theory correlation functions


