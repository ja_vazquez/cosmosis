
# ia_z_powerlap module

## 

**Name**: ia_z_powerlap

**File**: cosmosis-standard-library/intrinsic_alignments/z_powerlaw/ia_z_powerlaw.py

**Version**: 1.0

**Author(s)**:

- CosmoSIS team
- Niall Maccrann

**URL**: 

**Cite**: 



**Rules**: 

- 

**Assumptions**:

- Modify the intrinsic alignment power spectra to have a power-law dependence in (1+z)

**Explanation**

 Basic models of intrinsic alignments assume a specific simple evolution with redshift. This module takes an existing model of the IA power spectra and modifies them by giving them additional evolution in z.

Specifically, it takes P_II and P_GI (e.g. as calculated by the la_model module) and modifies them to:

P_II(k,z) -> (1+z)^alpha P_II(k,z) P_GI(k,z) -> (1+z)^alpha P_GI(k,z)




##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------


##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
intrinsic_alignment_parameters|alpha|Real; power law index to apply.
|z|real vector; redshift values of P(k,z) samples
|k_h|real vector; k values of P(k,z) samples in units of Mpc/h
|P_II|real 2d array; spectrum of intrinsic-intrinsic power at samples in (Mpc/h)^{-3}
|P_GI|real 2d array; spectrum of shear-intrinsic power at samples in (Mpc/h)^{-3}

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
intrinsic_alignment_parameters|P_II|real 2d array; modified spectrum of intrinsic-intrinsic power at samples in (Mpc/h)^{-3}
|P_GI|real 2d array; modified spectrum of shear-intrinsic power at samples in (Mpc/h)^{-3}
|z|real vector; redshift values of P(k,z) samples (replaced for technical reasons but unmodified)
|k_h|real vector; k values of P(k,z) samples in units of Mpc/h (replaced for technical reasons but unmodified)


