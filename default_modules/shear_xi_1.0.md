
# shear_xi module

## Compute the likelihood of a tomographic shear correlation function data set

**Name**: shear_xi

**File**: cosmosis-standard-library/likelihood/shear_xi/xipm_like_interface.py

**Version**: 1.0

**Author(s)**:

- Niall Maccrann

**URL**: 

**Cite**: 



**Rules**: 

- 

**Assumptions**:

- Gaussian likelihood of shear xi
- Redshift distribution used correctly matches tomographic bins

**Explanation**

Several surveys are measuring the cosmic shear 2-pt correlation functions xi_+(theta)  and xi_minus(theta) of the gravitational lensing in tomographic redshift bins.

This module calculates the likelihood of theoretical xi(theta) values values given a data set.  It assumes a simple Gaussian likelihood, so the only complexities are:

   selecting exactly which data to use - angular ranges, whether to use xi_minus

   if the covariance matrix came from simulations this

     adds noise to the covariance matrix - this the number of sims can be set




##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
data_file|string, file containing the data vector. Each row should be one angle, and then the ordering for e.g. three bins is: xip(0,1) xip(0,2) xip(1,1) xip(1,2) xip(2,2) xim(0,0) xim(0,1) xim(0,2) xim(1,1) xim(1,2) xim(2,2)
covmat_file|string, file containing the covariance matrix, either text file or numpy npy file. Ordering goes down the columns of the data vector first.
n_z_bins|integer, number of redshift bins to use
cov_num_rlzn|integer; default=0, number of realizations to assume for covariance simulations. Zero means no simulation errors.
plus_only|boolean; default=False, whether to use only xi+ and ignore xi-
theta_mins|string; default=empty,  Comma separated minimum angles in arcmin for the different bin pairs
theta_maxs|string; default=empty,  Comma separated maximum angles in arcmin for the different bin pairs

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
shear_xi|theta|1D real array, theta values of theory correlation functions
|xi_plus_i_j|xi_plus meausurements for i,j=(1..n_z_bins)
|xi_minus_i_j|xi_minus meausurements for i,j=(1..n_z_bins)

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
likelihoods|xipm_like|real, likelihood of supplied theory correlation functions


