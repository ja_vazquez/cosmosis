
# JulloLikelihood module

## Likelihood of Jullo et al (2012) measurements of a galaxy bias sample

**Name**: JulloLikelihood

**File**: cosmosis-standard-library/likelihood/jullo_bias/jullo.py

**Version**: 2012

**Author(s)**:

- Lucy Clerkin
- CosmoSIS Team

**URL**: http://www.sdss3.org

**Cite**: 

- http://arxiv.org/abs/1202.6491

**Rules**: 



**Assumptions**:

- COSMOS survey galaxy samples

**Explanation**

Galaxy bias refers to the relative density of galaxies compared to underlying dark matter, and can be a function of scale and/or redshift.

Jullo et al made measurements of galaxy bias for high and low mass samples.

This module compares a predicted b(z) or b(k,z) from theory to these measurements.


##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
mass|string, low or high.  Choose which Jullo sample to work with

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
bias_field|z|1D real array, redshift of theory samples
|k_h|1D real array, optional, wavenumber of samples in k_h. if not present then b(z) only is assumed
|b|1D or 2D real array, bias as a function of either k and z or just z

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
likelihoods|JULLO_LIKE|real, likelihood of supplied bias model


