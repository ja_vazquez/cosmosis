
# CRL_Eisenstein_Hu module

## Komatsu's CRL code to compute the power spectrum using EH fitting formula.

**Name**: CRL_Eisenstein_Hu

**File**: cosmosis-standard-library/structure/crl_eisenstein_hu/nowiggle_module.so

**Version**: 1

**Author(s)**:

- Komatsu-CRL

**URL**: http://www.mpa-garching.mpg.de/~komatsu/crl/

**Cite**: 

- http://www.mpa-garching.mpg.de/~komatsu/crl/
-  Eisenstein and Hu, ApJ, 496, 605 (1998)

**Rules**: 

- 

**Assumptions**:

- DEPENDENCIES: You need to run a module to compute the growth rate before this one.

**Explanation**

This module uses Eiichiro Komatsu's CRL code to calculate the power spectrum without BAO in it following Eisenstein and Hu, ApJ, 496, 605 (1998).  This is faster but less accurate than a Boltzmann code like CAMB.

The CosmoSIS seam modified this slightly to remove some copyrighted  N*merical R*cipes code. 


##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
zmin|Real, min value of redshift to save P(k,z) (default = 0.0)
zmax|Real, max value of redshift to save P(k,z) (default = 5.0)
nz_steps|Integer, number of steps used between zmin-zmax (default = 800)
kmin|Real, min value of k_h (Mpc/h) to save P(k,z) (default = 1.0*10^-5)
kmax|Real, min value of k_h (Mpc/h) to save P(k,z) (default = 10.0)
nk_steps|Integer, number of steps used between kmin-kmax (default = 800)

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
growth_parameters|d_z|1D real array, linear growth factor D
|f_z|1D real array, linear growth rate f
|z|1D real array, redshift of samples 
cosmological_parameters|omega_b|real scalar, baryon content
|omega_m|real scalar, matter content
|w|real scalar, dark energy EoS.  Optional; default=-1
|h0|real scalar, Hubble/100km/s/Mpc
|n_s|real scalar, scalar spectral index
|n_run|real scalar, scalar spectral index running. Options; default=-1
|a_s|real scalar, primordial amplitude

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
matter_power_no_bao|z|1D real array, redshifts of samples
|k_h|1D real array, k wavenumbers of samples in Mpc/h
|p_k|2D real array, matter power spectrum at samples in (Mpc/h)^-3


