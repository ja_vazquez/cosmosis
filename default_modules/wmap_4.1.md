
# wmap module

## Likelihood function of CMB from WMAP

**Name**: wmap

**File**: cosmosis-standard-library/likelihood/wmap7/wmap_interface.so

**Version**: 4.1

**Author(s)**:

- The WMAP Collaboration

**URL**: 

**Cite**: 

- Larson, D., et.al., 2011, ApJS, 192, 16L
- Komatsu, E., et.al., 2011, ApJS, 192, 18K

**Rules**: 

- "Likelihood released by WMAP Collaboration and packaged by cosmosis team.   Contact the latter with problems in the first instance. "


**Assumptions**:

- WMAP 9 year data

**Explanation**

The Wilkinson Microwave Anisotropy Probe measured the temperature and polarization of the CMB over the full sky in the K, Ka, Q, V, and W microwave bands.  

The WMAP produced this likelihood code, which takes in theory spectra for TT, EE, BB, and TE spectra and compares it to WMAP data.

The method used for the likelihood is different in different ell regimes and for different spectra. 


##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------


##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
cmb_cl|ell|Integer vector of angular frequencies for CMB spectra
|tt|Double vector of temperature spectra in l*(l+1) C_ell / uK^2 (if using TT data)
|ee|Double vector of E-mode polarization spectra in l*(l+1) C_ell / uK^2 (if using pol data)
|bb|Double vector of B-mode polarization spectra in l*(l+1) C_ell / uK^2 (if using pol data)
|te|Double vector of cross spectra in l*(l+1) C_ell / uK^2 (if using pol data)

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
likelihoods|wmap7_like|Combined log-likelihood from all WMAP components


