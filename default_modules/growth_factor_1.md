
# growth_factor module

## returns linear growth factor and growth rate for flat cosmology with either const w or variable DE eos w(a) = w + (1-a)*wa

**Name**: growth_factor

**File**: cosmosis-standard-library/structure/growth_factor/interface.so

**Version**: 1

**Author(s)**:

- CosmoSIS Team

**URL**: 

**Cite**: 



**Rules**: 

- If you use a file from a particular survey you should cite that survey

**Assumptions**:

- linear growth factor and rate in flat cosmology

**Explanation**

This simple module calculates the linear growth factor D, and linear growth rate, f, for flat cosmology with either const w or variable DE eos w(a) = w + (1-a)*wa.  Where D, f are defined by the growth of a linear perturbation, delta, with scale factor a: delta(a') = delta(a)*(D(a')/D(a)) and f = dlnD/dlna


##Parameters

These parameters can be set in the module's section in the ini parameter file.  
If no default is specified then the parameter is required.

Parameter | Description
------------|-------
zmin|Real, min value to save f,D (default = 0.0)
zmax|Real, max value to save f,D (default = 3.0)
dz|Real, redshift binsize (default  = 0.01)

##Inputs

These parameters and data are inputs to the module, either supplied as parameters by the sampler or
computed by some previous module.  They are loaded from the data block.

Section | Parameter | Description
--------|-----------|-------
cosmological_parameters|omega_m|real, baryon + cdm density fraction today
|w|real, w(z=0) equation of state of dark energy (default -1.0) 
|wa|real, equation of state parameter w(z) = w_0 + w_a z / (1+z)  (default 0.0)

##Outputs

These parameters and data are computed as outputs from the module

Section | Parameter | Description
--------|-----------|-------
growth_parameters|d_z|1D real array, linear growth factor D
|f_z|1D real array, linear growth rate f
|z|1D real array, redshift of samples 


