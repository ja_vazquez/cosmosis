# Changes

## Development version

The development version contains changes that have not yet been tested well enough to be added to the main version.  It has more features than numbered versions but you should also expect more bugs.  See the [development](development) page for more details.

## Version 1.3

## Sampling
 - Fisher matrix sampler
 - New GaussianLikelihood superclass to help add Fisher-compatible Gaussian likelihoods


##Standard Library Additions

 - Planck 2015 Likelihood
 - The Dark Energy Survey Science Verification Weak Lensing 2-point likelihood, including:
    * Photometric redshift bias marginalization
    * Shear bias marginalization
    * Power law intrinsic aligment models
    * xi(theta) likelihood
 - Add a utility to delete data block sections for more complex pipelines
 - Add matter and galaxy 2D C_ell spectra, and magnification, as well as their cross-correlations


##Standard Library Updates

 - Allow extrapolate_power to work downwards to kmin as well as kmax
 - Allow switching off distance output in FrankenEmu
 - Modify several likelihoods to support Fisher matrices
 - Fix linking on OSX Yosemite

##Standard Library Bug Fixes

 - Fix Planck Makefiles to work in Ubuntu - thanks to Matteo Costanzi
 - Fixed the "root" option
 - Improvement to customizability of various modules
 - Fix CAMB slowdown when P(k) not needed by exposing kmax parameter
 - Change how LAPACK is linked to enable easier custom linking
 - Fix a bug where the wrong values was used in BBN
 - Fix a compile error in the fgas module under newer gcc versions

## Core Features

 - Lock output files when they are created to raise an error whey you accidentally leave out the --mpi flag or run two jobs with the same output file name.
 - Added a collection of physical constant values for use in modules (list taken from astropy.constants). In python: "import cosmosis.constants".  In C: "#include cosmosis_constants.h".  In Fortran: "#include cosmosis_constants.fh".
 - Save priors in the head of the output file
 - Allow the grid sampler to save its outputs progressively
 - Warn if more than 1 million grid points requested in the grid sampler
 - Enable saving chains in FITS format as well as text format
 
## Postprocessing

 - Enable adding extra derived parameters during postprocessing with user-defined functions
 - Add weighted MCMC scatter plots
 - Added lower and upper 95% limit statistics
 - Enable postprocessing importance samples
 - Enable plotting multiple different sampler outputs in one plot
 - Enable plotting only some of the chain parameters
 - Select whether you want transparent or opaque contours
 - Add scatter plot for weighted MCMC
 - Fix unusual factor of 100 appearing in some scatter plots
 - Add options to postprocess only a subset of 2D plots to save time
 - Fix a crash when plotting only one likelihood in P(k) test sampler plots (thanks to Tom McClintock)
 - Enable postprocessing FITS chains
 - Fix a bug when postprocessing PMC chains


## Core Bug Fixes

 - Fix burning a fraction of the chain rather than a fixed number of samples when postprocessing
 - Fix a crash when one column was an exact linear function of another
 - Fix a bug that gave wrong MCMC median values



## Version 1.2


###Samplers

 - Added Population Monte Carlo sampler
 - Added Snake grid sampler
 - Added Kombine ensemble sampler
 - Added List simple sampler
 - (Experimental) Enable making a graphical representation of a pipeline in the test sampler
 - Improved documentation pages of all the samplers

###Postprocessing

 - Add blinding option to post-processing
 - Enable postprocessing directory of cosmology results
 - Allow postprocessing MCMCs with weight columns
 - Fix which columns are plotted in post-processing
 - Add flag to swap x and y axes in plots

###Standard Library Updates

 - Added MGCAMB
 - Added FrankenEmu Cosmic Emulator 
 - Added Nicaea code for lensing C_ell -> xi(theta)
 - Added Lucy Clerkin bias code 
 - Added Wrapper for CLASS Boltzmann code
 - Added Cluster f_gas likelihood by Adam Mantz 
 - Added BBN YHe consistency module by Adam Mantz
 - Added Balmes-Corasaniti strong lensing likelihood 
 - Added Suyu strong lensing likelihood 
 - Added WiggleZ BAO likelihood 
 - Updated to new version of isItGR
 - Added CAMB Jan 2015
 - Added a new format mode des_fmt to photo-z loader
 - Enable multiple renames in rename module
 - Enable using different consistency relation file in consistency module


###Standard Library Bugs

 - Fixed linear alignment code
 - Fixed bug with covariance being wrong for CFHTLenS
 - Fixed error when z_min>0 in photo-z distributions
 - Fixed some compilation problems with Planck likelihood v1
 - Fix nan errors when A=0 in intrinsic alignments

###Installation

 - Installer now works on Yosemite
 - Installer should be easier to update in future

###Infrastructure

 - Improve datablock logging facilities
 - Fix bugs with priors not working properly
 - Improved repository creation tool
 - Added an error reporting tool
 - Added a tool to download an existing module from a git/hg url
 - Added a tool to check for uncommitted changes



##Version 1.1

There are two changes  in v1.1 is that might break your runs: 

 - you now *should* specify --mpi when running multinest in MPI mode
 - you may need to change some double parameters (like some of the planck nuisance params) from integers by adding .0 on the end

### Standard library 

* Include small heliocentric correction to JLA supernova likelihood
* Add isItGR modified gravity camb
* Add newer Takahashi Halofit
* Add module to log-linearly extrapolate matter power spectra
* Add intrinsic aligments to weak lensing shear spectra
* Add Linear Alignment model for intrinsic weak lensing alignments in several variants (Bridle & King, Bridle & King Corrected, and Kirk, Rassat, Host & Bridle)
* Fix bugs stopping BBN likelihood from working at all
* Add a PPF shear spectra calculation
* Add utility functions to Limber approximation library 
* Increase accuracy of C_ell -> xi(theta) code
* Add module that lets you sample in sigma_8 by re-scaling the output from CAMB.
* Add mass functions from the CRL with numerical recipes replaced

### Samplers

* Fix multinest MPI output problem where part of file overwritten
* Enable saving data outputs in grid sampler 
* Fix bug saving additional output like sigma_8 from multinest
* Let test sampler save as tgz output

### Demos
* Add new demos and split off longer demos into "examples" directory
* Tweak amount of output in demo 6.
* Use PLANCK_DATA_DIR environment variables in Planck demos
* Demo 10: Smith Halofit versus Takahashi Halofit (plotting multiple ini files)
* Demo 11: Modified gravity effect on shear spectra (grid sampling saved as tgz)
* Demo 12: Extreme value stats for cluster masses
* Example A: Old demo 10, moved.
* Example B: Sample CFHTLenS with intrinsic aligments and sigma_8 (compare two models)
* Example C: Multinest Planck (saving to sql?)

### Infrastructure

* Allow environment variables in ini files
* Update self-updater code to be able to checkout development version
* Enable saving multiple 2D grids in Fortran
* Enable saving datablocks to single tgz files 
* Fix saving/loading metadata after finishing the chain in text output files
* Enable using environment variables in ini files
* Enable integer parameters in values files being passed to pipelines

### Post-processing
* Fix plotting grid samples in cases with a very large range in likelihoods
* Fix bug in Dunkley power spectrum testing printing repeated values
* Stop grid plots from including 2D plots of likelihood versus parameter
* Output all citations required by the pipeline




## Version 1.0

Version 1.0 is the first fully released version of cosmosis.  

If you have a module for a pre-release version then you may need to make minor changes to it.  The main is that you may need to change how python modules import cosmosis features:

```
#!python
#Old:
from cosmosis import names, option_section
#New:
from cosmosis.datablock import names, option_section
```

If you have developed module for the prototype *des-pipe* code then the interface functions have been changed significantly.  See the documentation linked from the [modules page](modules) for more information.