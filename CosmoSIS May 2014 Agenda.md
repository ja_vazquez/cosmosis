# Agenda #

http://cfcp.uchicago.edu/events/kicp_workshops-2014.html#id_428

We are delighted that this meeting brings together such an enthusiastic group of scientists, and as a result we want to keep scheduled talks to a minimum, so we can all learn by doing, and so we can all shape the agenda according to the interests of the participants. 
The talks on the first morning will be a brief overview of the system, and in the afternoon we propose to quickly move to active coding sessions with the past-cosmosis developers in the room (we hope by the end of the meeting all the participants will be cosmosis developers!).
The second morning will be in "unconference" format, where participants propose topics for small group discussion/talks/working, in real time.
Finally we would like to finish the workshop by bringing together some of the work that was started and making a plan for how to keep helping each other to get more out of cosmosis.
So do bring along your ideas for tools you would like to use, we're hoping you'll find cosmosis useful for writing your next (or current!) paper.

```
Thu
9:00 Welcome
9:05 Talk: Overview and vision for CosmoSIS
9:15 Hands-on: Installation of CosmoSIS, running the demos
10:00 Talk: Under the hood
10:10 Discussion
10:15 Coffee
10:45 Talk: How to add a new module: How to talk to the rest of CosmoSIS
10:55 Talk: How to add a new module: Worked example using CFHTLenS data 
11:05 Talk: How to add a new module: Where to put it, and how to get credit
11:15 Discussion
11:30 Open floor: Brainstorm of new modules that could be added
11:45 Prioritisation of modules and getting into teams
12:15 Lunch
14:00 Hands-on: Writing new modules in teams
15:15 Open floor: Update on progress and sharing experiences so far
15:30 Coffee
16:00 Hands-on: Writing new modules continued
17:00 Open floor: Update on progress and goals for tomorrow
17:15 Planning for the unconference tomorrow
17:30 Close


Fri
9:00 Unconference (sessions schedule live on what people want to contribute/ hear about) (can include continuing working on modules)
10:15 Coffee
10:45 Unconference
12:00 Reality check: What can we realistically finish this afternoon?
12:30 Lunch
14:00 Hands-on: Finishing stuff 
15:00 Reality check: What would we like to do going forward, after this meeting
15:30 Coffee
16:00 Hands-on: Last chance to finish stuff
17:00 Plans going forward
```