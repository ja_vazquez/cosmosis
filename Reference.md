# CosmoSIS Reference

The main CosmoSIS code (as distinct from the science modules) is primarily designed to be used as a command line program, but you might need to delve in and modify or re-use various parts of it.

You might want to do this if:

 - you are writing a new sampler
 - you want to customize plotting, post-processing, or the output of chains
 - you want to have a new way of setting up a likelihood pipeline
 - you want to use a CosmoSIS pipeline for another purpose
 - you just like reading code

# Samplers

You create a CosmoSIS sampler by making a subclass ```cosmosis.samplers.Sampler``` or ```cosmosis.samplers.ParallelSampler```, typically in a subdirectory of cosmosis/samplers.

New sampler subclasses must be called XYZSampler, where XYZ is the name of the sampler.  This ensures that when a new Sampler subclass is defined and imported to samplers/__init__.py then it is automatically registered with CosmoSIS, so that if the user writes ```sampler=XYZ``` in their ini file runtime section then the right sampler is used.

## Methods available to Sampler

The Sampler base class provides one method for subclasses to use:

    Sampler.read_ini(self, option, option_type, default=None)

```option``` is the name of an option to read from the confgiuration ini file for the sampler. ```option_type``` is a python type, one of ```float, int, bool, str``` which decides what type of option to look for. ```default``` is the default value to use if the option is not specified by the user (```None``` means that no default is used and an error should be raised if the option is not set).


The ParallelSampler subclass provides two more methods for subclasses:

    Sampler.is_master(self)
    Sampler.worker(self)

The ```is_master``` method simply returns True if the sampler is at MPI rank zero in the pool of processes.

The ```worker``` method, which may be overridden by subclasses but often does not need to be, is called by all the non-master processes instead of the ```execute``` function below. It simply tells the process to wait for a function to be sent from master to run.


## Methods a sampler subclass must implement

### Sampler.config(self)

### Sampler.execute(self)

### Sampler.is_converged(self)

The interface a sampler must implement.

# Postprocessing

The structure of postprocessors and their elements, including plotting and statistics.
Often easier to use tweaks or extra plots.
This may change soon.

# Output

The interface a new output must implement.
The existing output modes.

# Parameter

The parameter class, initializing it, normalization, name scheme.

# Module

The module class - describe functions for intitialzing and running.

# Pipeline

The pipeline class, and in particular how it is created and loaded.
The various methods for running the pipeline.

# Inifile

The inifile class, reference the python ConfigParser docs, and describe extensions to it.

# Prior

The prior class, and creating new priors.

# MPI Pools

The MPI pool's behaviour and using it elsewhere.  Limitations including the pickle issue.

# Datablock

The datablock - probably do not go into this in too much detail since
a) it's a very in-depth topic
b) it is hard to modify
c) most of the user-facing methods should be documented elsewhere.