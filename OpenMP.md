# OpenMP to speed up modules

OpenMP is a collection of functions for C, C++, and Fortran programs to enable simple shared-memory parallelization.  This means that different processors on the computer split up a task - for example, a loop of calculations from i = 1 .. 10 can be split up so that one processor does odd i and the other even.

# OpenMP in cosmosis

The cosmosis samplers do not use OpenMP, but CosmoSIS modules can, and some do.  For example, CAMB uses OpenMP and each processor does calculations for different wavenumbers (k values).

To enable OpenMP for all the modules which use it, compile cosmosis with the environment variable COSMOSIS_OMP set to one.  Then run with the variable OMP_NUM_THREADS set to the number of threads you want to use.  For example, this will run demo 1 using CAMB's OpenMP parallelization:

```
#!bash

#You need only do these bits once
make clean
export COSMOSIS_OMP=1
make

#If you do not set this variable it will use all the cores.
export OMP_NUM_THREADS=4
cosmosis demos/demo1.ini

```


# OpenMP and MPI

OpenMP and MPI can be used at the same time.  Compile as shown above, and then [run MPI as normal](mpi).  In this case, though, you must set the environment variable OMP_NUM_THREADS carefully.  Each MPI process will try to use OMP_NUM_THREADS threads.  But if you run MPI with n_mpi processes, then the total number of threads will be (n_mpi * OMP_NUM_THREADS).  If this is more than the number of cores you have on your machine then the thing will slow down hugely.

For example, on a laptop with four cores, you could use 2 processes and 2 threads per processes:

```
#!bash
export OMP_NUM_THREADS=2
mpirun -n 2 cosmosis --mpi params.ini
```