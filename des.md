# Dark Energy Survey Code

If you are a member of the DES collaboration there is a private repository you can access containing DES-specific code.  As a member you can upload your own code to this repository, or use other peoples.



The bootstrap script described on the installation page will download it for you if you give it the "-e des" flag when you run it e.g. 

```
#!bash
    ./cosmosis-bootstrap -d -e des cosmosis
```

see the [installation page](https://bitbucket.org/joezuntz/cosmosis/wiki/Manual%20Install) for more info.  It will be in the directory cosmosis-des-library

Otherwise you can get it by running this from the main cosmosis directory:

```
#!bash
    git clone https://darkenergysurvey@bitbucket.org/joezuntz/cosmosis-des-library
```
and when prompted enter the collaboration password.

The cosmosis-des-library directory will then contain DES modules.

Type 
```
#!bash
    make
```
to compile the DES modules with CosmoSIS.

Examples of runs using these modules are in the cosmosis-des-library/demos directory.