# Demo 2:  Getting Planck and BICEP likelihoods for a single cosmology #

## Running ##

Before you run this demo, you will need to [download the Planck data here](http://pla.esac.esa.int/pla/aio/product-action?COSMOLOGY.FILE_ID=COM_Data_Likelihood-CAMspec_R1.10.tar.gz) unless you have just installed it during the bootstrap phase adding the -d option in `./cosmosis-bootstrap -d`.

After you have downloaded the file, edit demos/demo2.ini and change the line:
t_high_file = 
to specify the path to the file you just downloaded.

Once you have saved demo2.ini you can run:

```
#!bash

cosmosis demos/demo2.ini
```

You should seem some output from CAMB, just like in demo1.  At the end you will also see the total likelihood value of the cosmology.

This time the output directory is called demo_output_2.  You can look at the individual likelihood values by looking at the file:

```
#!bash

 cat  demo_output_2/likelihoods/values.txt

```

As before, you can also plot it using:

```
#!bash

 postprocess demos/demo2.ini -o plots -p demo2

```
or, with R:
```
#!bash

 ./cosmosis/plotting/cosmology_theory_plots.r demo_output_2 -o plots -p demo2

```

You will now have a collection of png plots in the plots directory.  This time they will include a BB plot, which we need for BICEP:

![demo2_bb.png](https://bitbucket.org/repo/KdA86K/images/1594462742-demo2_bb.png)
## Understanding ##

Once again, the pipeline is defined in the section of that name

```
#!ini

[pipeline]
modules = consistency camb planck bicep2
values = demos/values2.ini
; ...
likelihoods = bicep planck

```

This time we are running four modules - consistency, camb, Planck, and Bicep2.  You can see the paths to them in the sections below.  The latter two of these are interesting in that they generate likelihood values, which can be used by MCMC samplers to explore the parameter space.  We have also told cosmosis what likelihoods it should look for by setting:
likelihoods = bicep planck

This means that CosmoSIS will look for two values, bicep_like and planck_like in the likelihood section when it comes to work out the total likelihood.  There may be other likelihoods calculated (for example, the file demo_output_2/likelihoods/values.txt contains all the separate Planck contributions as well as the total), but if they are not in this list they will *not* be included.


Now have a look at the **demos/values2.ini** file.  You will see that we now have to different sections to split up the parameters we use by type.  We have the regular cosmological parameters (now including r_T=0.2 as we are in the exciting post-BICEP world), but we also have the many nuisance parameters that Planck needs to know.