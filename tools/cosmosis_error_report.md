#CosmoSIS Error Report

The *cosmosis_error_report* tool helps you collect together information to report an error when running CosmoSIS.  It won't help much with scientific errors - it is mainly designed for code problems.

If you normally run:

```
#!bash

    cosmosis parameter_file.ini
```

then you can change that to:

```
#!bash

    cosmosis-error-report parameter_file.ini
```


This will collect various pieces of information about your system. It makes a text file listing:

 - the files in the CosmoSIS directory and subdirectories (not the file contents)
 - any changes you have made to CosmoSIS files
 - all environment variables 
 - the ini files you are running on
 - the standard output of the run

If there's any *secret* information in any of these things, for example if you have foolishly set an environment to contain a password in, or have a file whose very name is secret, like GR_is_wrong.txt, then you shouldn't send the results to anyone.

The program will create a zipped text file and open a web page where you can file an issue about your problem, if you wish.

You can run:

```
#!bash

	cosmosis-error-report --help
```

to get information on the other options the program provides:

```
#!text

    --mpi MPI   Run under MPI with this many processes (you do *NOT* need to use
                mpirun - this script will do that for you).

    --no-web    Do not open a web browser to create an issue at the end, for
                example if you are running remotely on a cluster.

    --no-zip    Do not zip up the output at the end, in case you want to look at
                it first.
```
