#CosmoSIS Get

It is a very, very good idea to store your work, including new code modules you are writing, in an online version controlled repository, for example one from GitHub or BitBucket.

The cosmosis-get script helps you download an existing module that you or someone else has put
in a repository.  If you want instead to make a new module and repository, then use the 
cosmosis-new-module tool instead.

To use this script you'll need to know the web address of the module you want to download,
for example:

```
#!bash

    cosmosis-get http://github.com/my_friends_user_name/my_friends_module
```

By default the module (if it can find it) would be downloaded to a directory modules/my_friends_module.  You can put it somewhere else using the -d flag to specify a different directory, for example:

```
#!bash

    cosmosis-get -d modules/my_modules/module1 http://github.com/my_friends_user_name/my_friends_module
```

The only other option is --hg, which you can use if your module is in a Mercurial repository rather than git (ask the person who made the module if you don't know):

```
#!bash

    cosmosis-get --hg http://bitbucket.org/username/my_hg_repo
```

