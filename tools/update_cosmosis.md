# Update CosmoSIS

CosmoSIS releases new numbered versions when new modules and features are added.  A "bleeding edge" version, with many new improvements and features but which is not yet fully tested, is also available.  The update-cosmosis tool switches you between versions. It modifies the main cosmosis repository and the standard library.

If you want a specific numbered version, say 1.1 (for example to repeat an old analysis) you can update to a specific version:

```
#!bash

    update-cosmosis 1.1
```

Or if you run without any options it will suggest updating to the newest version (if there is one newer than the one you are on), and tell you what versions are available:


```
#!text
    You are currently at version 1.1 of cosmosis
    The most up-to-date version of cosmosis is v1.1
    All versions I know about:
        v1.0
        v1.1
    
    You are already at the latest release of cosmosis.
    
    To update to a different version please run the script
    again with an argument saying what version you want, e.g.
    update-cosmosis v1.0
    
    OR you can get the bleeding edge with:
    update-cosmosis --develop
```

You can also update to the bleeding edge development version:

```
#!bash
    update-cosmosis --develop

    Updating to the newest changes in the development branch.
    ...
```

If you try to update when you have made changes then the script will not do anything but will give you some suggestions:


```
#!text
    You seem to have made changes to the files in this directory.
    You should have a look at what files have changed with the command:
        git status --untracked=no
    and at what the changes are with:
        git diff
    
    Then if you are okay to lose ALL these changes, please run:
       git checkout .
    in the main cosmosis dir, then run this script again.
    Otherwise I'm afraid you're going to have to learn a bit about git
    
    I have NOT updated anything - please read the info above
```


One warning - we sometimes update the update-cosmosis script.  If you go back to an earlier version of the script then its function may have changed.  For example, the v1.0 updater does not have a --develop option, and instead has an obsolete --master option.
