# CosmoSIS Check Modules

CosmoSIS is made up of two separate parts, the main cosmosis core, and the cosmosis-standard-library.  Each of these is a separate *repository* - the code in them is managed separately and stored in different places on the website that hosts them.

You might also have made your own repositories, for example using the cosmosis-new-module tool.

This script helps you by searching through your cosmosis directory to find as many repositories as possible (by looking for directories with a subdirectory called .git), and prints out the status of them - for example, whether they have unsaved changes, changes saved locally but not "pushed" up to a website, or don't have a remote website set up to host them at all.

There are no flags for this module; you just run it from anywhere.  Here's some example output on my machine:

```
#!text
    > cosmosis-check-module

    These directory have repositories in, but they are not set up to save
    To a remote website or similar:
         /Users/jaz/src/cosmosis/modules/cosmolike2
    
    These directories have unsaved changes in; you need to git commit in them
    (and then git push to save to a web repository)
         /Users/jaz/src/cosmosis
    
    These directories may have changes saved locally in *branches* other than the current one
    (You might want to 'git push <BRANCH_NAME>' in them - you'll need to figure out the branch name first.):
         /Users/jaz/src/cosmosis
         /Users/jaz/src/cosmosis/cosmosis-standard-library
```
