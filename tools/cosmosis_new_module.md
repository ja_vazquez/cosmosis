# CosmoSIS New Module

The script cosmosis-new-module leads you through creating a repository for a new CosmoSIS module.  If you're comfortable with using "git" then you probably don't need this module - it is mostly just a git wrapper.

Say you wanted to make a new project called "theory" with two module in it, "linear" and "nonlinear".  You would create space for these like this:

```
#!bash
    cosmosis-new-module theory linear nonlinear
```

This command will:

 - create a directory for your project and subdirectories for your modules, with mostly empty Makefiles in.
 - start a git repository in the new project directory.
 - add the new project directory to the file modules/Makefile.modules so when you type "make" in cosmosis your project is included.
 - Display instructions on what to do next if you want to save your repository on the web as well as just your computer.