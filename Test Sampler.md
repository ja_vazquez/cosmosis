-----------------------------------------------

# Test Sampler

The Test Sampler evaluates the CosmoSIS pipeline at a single point in parameter space and is useful for ensuring that the pipeline has been properly configured.  It is also commonly used to examine (say the set of parameters returned by the [MaxLike Sampler](maxlike). Example using the Test Sampler to compute standard cosmological quantities are given in [Demo 1](Demo1) and [Demo 2](Demo2).

[//]: # (This is a test comment and should not appear)
#Usage
```
cosmosis [ini]
```
Note: the TestSampler cannot be run in parallel.

# ini file options

The Test Sampler uses a standard CosmoSIS _ini_ file with [pipeline], [output] and [module] interface sections together with the following
```
[runtime]
sampler = test

[test]
fatal-errors=[boolean]
save_dir=[output directory]
```
After execution, [output directory] will contain any data products generated during pipeline execution. If fatal-errors is set, any exceptions will cause the sampler to exit immediately.

# Setting sample parameters

The pipeline is evaluated at the start values for each parameter defined in _values.ini_.