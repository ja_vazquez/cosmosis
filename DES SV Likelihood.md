# Using the DES SV Likelihood #

The Dark Energy Survey Science Verification likelihood for the shear two-point measurements released on 21 July 2015 are now available in CosmoSIS, a package for parameter estimation in cosmology.

Here are the steps to using it.

## 1. Install CosmoSIS ##
[Follow these instructions](bootstrap) to install cosmosis (the download may take a while, depending where you are - sorry).


## 2. Get the development version ##
At the moment the DES likelihood is only available in the "development" (bleeding edge) branch of CosmoSIS, though this will change soon.

Run the commands:

    source config/setup-cosmosis
    update-cosmosis --develop
    make clean
    make

you will get the most up-to-date cosmosis version.

## 3. Test-run the likelihood ##

Then you can run an example of the DES-SV cosmic shear likelihood by doing:

    cosmosis examples/des_sv.ini


## 4. Explore further ##

This will just run a test, calculating the likelihood at a single point in parameter space.  To explore the likelihood you can change to a real sampler - details are in ``examples/des_sv.ini``, as are details on what is happening in the likelihood and how to modify and experiment with it. You can also see ``examples/des_sv_values.ini`` for the input parameters, and ``examples/des_sv_priors.ini`` for the priors.

If you are new to CosmoSIS please see our [front page](Home) for tutorials and reference information.