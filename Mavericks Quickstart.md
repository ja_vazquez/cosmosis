# Quickstart for mavericks #

* Install xcode and xcode command line tools 
```
#!bash
$> sudo xcode-select —install
```

* Create a new directory and enter it
* Download [the bootstrap script](https://bitbucket.org/joezuntz/cosmosis/downloads/cosmosis-bootstrap) into your current directory 
* Download [the manifest file for mavericks](https://bitbucket.org/joezuntz/cosmosis/downloads/d13-files.v00_00_02) into your current directory  
* Execute the following commands
```
#!bash
$>  chmod +x ./cosmosis-bootstrap 
$> mkdir $HOME/products
$> ./cosmosis-bootstrap -w d13-files.v00_00_02 $HOME/products
```
* Go and have lunch (especially while it is downloading the Planck data) (or remove the "-p" flag to get started without Planck data, and just get a cup of tea instead)
```
#!bash
$> source $HOME/products/setups
$> ups list -aK+
```

If everything worked then you should see something like the following:
```
"cfitsio" "v3_35_0" "Darwin64bit+13" "debug" "" 
"cfitsio" "v3_35_0" "Darwin64bit+13" "prof" "" 
"fftw" "v3_3_3" "Darwin64bit+13" "debug" "" 
...
"ups" "v5_0_5" "Darwin64bit+13" "" "current" 
"wmapdata" "v5_00" "NULL" "" "" 
```
If not, then you may need to go through the longer description of how to install the pre-requisites for _CosmoSIS_.

Now install _CosmoSIS_ by entering the following commands

```
#!bash
$> git clone https://sarah_bridle@bitbucket.org/joezuntz/cosmosis
$> cd cosmosis
$> git clone https://bitbucket.org/joezuntz/cosmosis-standard-library
$> source config/setup_for_dev.sh $HOME/products
$> make
```

You can check it works by trying the demos e.g. 
```
#!bash
$> cosmosis demos/demo1.ini
```

This works for me - Sarah 28 Apr 2014 !!!!

