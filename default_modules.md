## background

- [distances (2015)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/distances_2015)  Output cosmological distance measures for dynamical dark energy

## bias

- [clerkin (1)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/clerkin_1)  Compute galaxy bias as function of k, z for 3-parameter Clerkin et al 2014 model

## boltzmann

- [camb (Jan15)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/camb_Jan15)  Boltzmann and background integrator for BG, CMB, and matter power

- [camb (Nov13)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/camb_Nov13)  Boltzmann and background integrator for BG, CMB, and matter power

- [class (2.4.1)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/class_2.4.1)  Boltzmann and background integrator for BG, CMB, matter power, and more

- [extrapolate (1.0)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/extrapolate_1.0)  Simple log-linear extrapolation of P(k) to high k

- [Halofit (Camb-Oct-09)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/Halofit_Camb-Oct-09)  Compute non-linear matter power spectrum

- [Halofit_Takahashi (Camb-Nov-13)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/Halofit_Takahashi_Camb-Nov-13)  Compute non-linear matter power spectrum

- [isitgr-camb (1.1)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/isitgr-camb_1.1)  Modified version of CAMB to implement phenomenological modified gravity models

- [mgcamb (Feb14)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/mgcamb_Feb14)  Modified Gravity Boltzmann and background integrator for BG, CMB, and matter power

- [sigma_r (1.0)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/sigma_r_1.0)  Compute anisotropy dispersion sigma(R,z)

## intrinsic_alignments

- [linear_alignments (1.0)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/linear_alignments_1.0)  Compute the terms P_II and P_GI which go into intrinsic aligment calculations

- [ia_z_powerlap (1.0)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/ia_z_powerlap_1.0)  

## likelihood

- [WiggleZBao (1401.0358v2)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/WiggleZBao_1401.0358v2)  Compute the likelihood of the supplied expansion history against WiggleZ BAO data

- [BBN (PDG13)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/BBN_PDG13)  Simple prior on Omega_b h^2 from light element abundances

- [BICEP2 (20140314)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/BICEP2_20140314)  Compute the likelihood of the supplied CMB power spectra

- [BOSS (1303.4486)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/BOSS_1303.4486)  Compute the likelihood of supplied fsigma8(z=0.57), H(z=0.57), D_a(z=0.57), omegamh2, bsigma8(z=0.57)

- [CFHTLens (1.0)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/CFHTLens_1.0)  Compute the likelihood of CFHTLens tomographic data given input shear correlation functions

- [Extreme_Value_Statistics (1.0)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/Extreme_Value_Statistics_1.0)  PDF of the maximum cluster mass given cosmological parameters

- [Cluster_mass (1.0)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/Cluster_mass_1.0)  Likelihood of z=1.59 Cluster mass from Santos et al. 2011

- [fgas (2014)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/fgas_2014)  Likelihood of galaxy cluster gas-mass fractions

- [JulloLikelihood (2012)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/JulloLikelihood_2012)  Likelihood of Jullo et al (2012) measurements of a galaxy bias sample

- [planck (1.0)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/planck_1.0)  Likelihood function of CMB from Planck

- [planck2015 (2)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/planck2015_2)  Likelihood function of CMB from Planck 2015 data

- [Riess11 (2011)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/Riess11_2011)  Likelihood of hubble parameter H0 from Riess et al supernova sample

- [shear_xi (1.0)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/shear_xi_1.0)  Compute the likelihood of a tomographic shear correlation function data set

- [planck_sz (1.0)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/planck_sz_1.0)  Prior on sigma_8 * Omega_M ** 0.3 from Planck SZ cluster counts

- [wmap (4.1)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/wmap_4.1)  Likelihood function of CMB from WMAP

- [wmap (5)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/wmap_5)  Likelihood function of CMB from WMAP

- [wmap_shift (1.0)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/wmap_shift_1.0)  Massively simplified WMAP9 likelihood reduced to just shift parameter

## luminosity_function

- [Joachimi_Bridle_alpha (1.0)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/Joachimi_Bridle_alpha_1.0)  Calculate the gradient of the galaxy luminosity function at the limiting magnitude of the survey.

## mass_function

- [Press_Schechter_MF (1)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/Press_Schechter_MF_1)  Code to compute the PressSchechter mass function given Pk from CAMB, based on Komatsu's CRL

- [Sheth-Tormen MF (1)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/Sheth-Tormen MF_1)  Code to compute the Sheth-Tormen mass function given Pk from CAMB, based on Komatsu's CRL

- [Tinker_MF (1)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/Tinker_MF_1)  Code to compute the Tinker et al. mass function given Pk from CAMB, based on Komatsu's CRL

## number_density

- [gaussian_window (1)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/gaussian_window_1)  Compute Gaussian n(z) window functions for weak lensing bins

- [load_nz (1)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/load_nz_1)  Load a number density n(z) for weak lensing from a file

- [photoz_bias (1)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/photoz_bias_1)  Modify a set of loaded n(z) distributions with a multiplicative or additive bias

- [smail (1)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/smail_1)  Compute window functions for photometric n(z)

## shear

- [add_intrinsic (1.0)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/add_intrinsic_1.0)  Sum together intrinsic aligments with shear signal

- [cl_to_xi_nicaea (1.0)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/cl_to_xi_nicaea_1.0)  Compute WL correlation functions xi+, xi- from C_ell

- [shear_bias (1)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/shear_bias_1)  Modify a set of calculated shear C_ell with a multiplicative bias

- [wl_spectra (1.0)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/wl_spectra_1.0)  Compute various weak lensing C_ell from P(k,z) with the Limber integral

- [wl_spectra_ppf (1.0)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/wl_spectra_ppf_1.0)  Compute weak lensing C_ell from P(k,z) and MG D(k,z) with the Limber integral

## strong_lensing

- [balmes (1)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/balmes_1)  

- [suyu_time_delay (1)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/suyu_time_delay_1)  

## structure

- [CRL_Eisenstein_Hu (1)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/CRL_Eisenstein_Hu_1)  Komatsu's CRL code to compute the power spectrum using EH fitting formula.

- [FrankenEmu (2.0)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/FrankenEmu_2.0)  Emulate N-body simulations to compute nonlinear matter power

- [growth_factor (1)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/growth_factor_1)  returns linear growth factor and growth rate for flat cosmology with either const w or variable DE eos w(a) = w + (1-a)*wa

## supernovae

- [jla (3)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/jla_3)  Supernova likelihood for SDSS-II/SNLS3

## utility

- [BBN-Consistency (0705.0290)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/BBN-Consistency_0705.0290)  Compute consistent Helium fraction from baryon density given BBN

- [consistent_parameters (1.0)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/consistent_parameters_1.0)  Deduce missing cosmological parameters and check consistency

- [sigma8_rescale (1.0)](https://bitbucket.org/joezuntz/cosmosis/wiki/default_modules/sigma8_rescale_1.0)  Rescale structure measures to use a specified sigma_8

