# Metropolis sampler

# Overview

The Metropolis sampler is a simple Metropolis-Hastings MCMC sampler written by the CosmoSIS team.  It tries to approximately copy CosmoMC's style of sampling (though we don't yet have all its features!).

The proposal is along random rotations of the eigenvectors of the supplied proposal covariance matrix.

# Usage

Once the file params.ini describing the pipeline and options is in place you can run with, for example, one of these commands:

```
#!bash
> cosmosis params.ini
> mpirun -n 2 cosmosis --mpi params.ini

```

See the [page on MPI](mpi) for more details on using MPI.

# Parameters

**samples** is the total number of samples to be be taken by the chain.  If you are using MPI and there is more than one chain then each one takes this many samples at most

**nsteps** is the number of steps between each time that the results are output and convergence checked for

**covmat** this is optional but strongly recommended - provide a proposal covariance matrix as an nparam*nparam text file

**random_start** This is False if not set; if set to True the chain is initialized from a random point in the prior space.  This can be useful if you want to run multiple chains and see if they all find the same peak.

**Rconverge** This is only used if [MPI](mpi) is used and multiple chains are run.  The Gelman-Rubin test, which compares the different chains, is used with this stopping criterion.  The chains end if convergence is reached.  A value of Rconverge=0.01 usually ensures a well-converged chain.  This test is currently run independently on the different parameters, so you should use a smaller value than you would in CosmoMC.

