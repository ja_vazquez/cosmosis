# Welcome

Welcome to **CosmoSIS**.

CosmoSIS is a **cosmological parameter estimation code**.  It is now at **version 1.3**

It is a framework for structuring cosmological parameter estimation in a way that eases re-usability, debugging, verifiability, and code sharing in the form of calculation [modules](modules).

It consolidates and connects together existing code for predicting cosmic observables, and makes mapping out experimental likelihoods with a range of different techniques much more accessible

CosmoSIS is described in Zuntz et al: http://arxiv.org/abs/1409.3409 - if you make use of it in your research, please cite that paper and include the URL of this repository in your acknowledgments. Thanks!

##Mailing Lists##

Sign up for the _CosmoSIS_ announcements email list (for announcements by core team) by emailing listserv@fnal.gov with the subject line and message text: subscribe cosmosis_messages

Sign up for the _CosmoSIS_ users email list (for discussion among users) by emailing listserv@fnal.gov with the subject line and message text: subscribe cosmosis_users


## Install CosmoSIS

CosmoSIS needs a 64-bit operating system.

 - [Recommended: Automatic Installation on Mac OSX Mavericks (10.9), Yosemite (10.10), and Scientific Linux 6](bootstrap)

 - [Manual Installation on other operating systems](Manual Install)

## Try some short demos

These demos illustrate basic cosmosis concepts in action.

 - [Demo 1: Get standard cosmological functions for a given cosmology](Demo1)

 - [Demo 2: Get the Planck and BICEP likelihood for a given cosmology](Demo2)

 - [Demo 3: Get a slice through the BICEP likelihood in r_T](Demo3)

 - [Demo 4: Find the best fit cosmological parameters for a Planck likelihood](Demo4)

 - [Demo 5: Running an MCMC analysis of supernova data](Demo5)

 - [Demo 6: Get the CFHTLenS likelihood for a given cosmology](Demo6)

 - [Demo 7: Get a 2D likelihood grid from BOSS DR9 measurements of f*sigma_8 ](Demo7)

 - [Demo 8: Compare CAMB with Eisenstein & Hu; get growth factors; tweak plots](Demo8)

 - [Demo 9: Get Bayesian evidence with Multinest; make scatter and custom plots](Demo9)

 - [Demo 10: Compare constraints with two different halofit models for nonlnear power, sampling sigma_8 instead of A_s](Demo10)

- [Demo 11: Slice through constraints on modified gravity with isItGR and CFHTLenS; save full data for all grid points to file](Demo11)

- [Demo 12: Get the PDF for extreme cluster masses using the Tinker mass function](Demo12)

- [Demo 13: A fast grid sampling of the JLA supernovae using the Snake sampler](Demo13)

- [Demo 14: The Kombine sampler applied to cluster gas fraction measurements; advanced postprocessing options](Demo14)

- [Demo 15: More Galaxy Two-Point Correlation Functions](Demo15)

- [Demo 16: Robust maximum-posteriors with the Minuit sampler](Demo16)

- [Demo 17: A Fisher Matrix for the Dark Energy Survey SV cosmic shear](Demo17)


## Longer demos

These longer examples show realistic parameter constraint pipelines.

 - [Example A: Simple CosmoMC-like Metropolis-Hastings analysis for WMAP9](example_a)

  - [Example B: Multinest analysis of CFHTLenS with intrinsic alignments](example_b)

 - [Example C: Emcee analysis of an extreme mass cluster to constrain omega_m](example_c)


## Modules ##

All the scientific code in CosmoSIS is organized into independent *modules*.  CosmoSIS ships with a standard library of modules doing most standard cosmology calculations, and if you want to build or modify a pipeline you can use them:

 - [Standard library modules that come with CosmoSIS](default_modules)

 - [Understanding modules](modules)

 - [Creating new modules](creating_modules)

 - [Where to put your own modules](Where to put CosmoSIS-compatible code)

 - [Modifying CAMB - an example tutorial on merging a modified camb into CosmoSIS](camb_mod_tutorial)


## Input Files ###

A CosmoSIS run is specified with up two or three parameter files.  The first two are required and the
third (priors) is optional: 

 - [Building your pipeline with the parameter file](parameter_file)
 - [Setting input parameter ranges with the values file](values_file)
 - [Adding priors with the priors file](priors_file)


## Samplers ##

CosmoSIS comes with a range of samplers suitable for different likelihoods spaces.

Simple:

 - [test sampler](samplers/test) Evaluate a single parameter set
 - [list sampler](samplers/list) Re-run existing chain samples

 Classic:

 - [metropolis sampler](samplers/metropolis) Classic Metropolis-Hastings sampling
 - [importance sampler](samplers/importance) Importance sampling
 - [fisher sampler](samplers/fisher) Fisher Matrices
 
 Max-Like:

 - [maxlike sampler](samplers/maxlike) Find the maximum likelihood using various methods in scipy
 - [gridmax sampler](samplers/gridmax) Naive grid maximum-posterior

Ensemble:

 - [emcee sampler](samplers/emcee) Ensemble walker sampling
 - [kombine sampler](samplers/kombine) Clustered KDE
 - [multinest sampler](samplers/multinest) Nested sampling
 - [pmc sampler](samplers/pmc) Adaptive Importance Sampling
 
 Grid:

 - [grid sampler](samplers/grid) Regular posterior grid 
 - [snake sampler](samplers/snake) Intelligent Grid exploration

## Helper tools

 - [Self-Updater](tools/update_cosmosis) Get a newer/older version of CosmoSIS
 - [Error reporter](tools/cosmosis_error_report) Create a full error report to help diagnose problems
 - [Module Creator](tools/cosmosis_new_module) Create a new project and collection of modules
 - [Module Downloader](tools/cosmosis_get) Download a module from an internet repository
 - [Repository checker](tools/cosmosis_check_modules) Check for unsaved changes in all your module repositories

## Parallelize ###

 - [Using MPI to speed up sampling](mpi)

 - [Using OpenMP to speed up modules](OpenMP)


## Debugging modules ##

 - [Debugging C/Fortran/C++ modules](debugging_compiled)

## Libraries available to multiple modules ##
 - [2D Interpolator and Limber approximation library](liblimber) ( plus [notes on accuracy of interpolating P(k,z)](pk_interpolation))


## Reference

 - [Pre-defined data sections in CosmoSIS](default_sections)

 - [Configuring the pipeline](Pipeline)

 - [The _CosmoSIS_ Breakout Workshop May 2014](CosmoSIS May 2014)



## Survey-specific code

 - [Dark Energy Survey](des)

 - [DES SV Shear 2 point likelihood](DES SV Likelihood)


## Changes

 - [Log of past changes](changelog)
 - [Development version](development)
 - [Roadmap of future plans](roadmap)

## Discussion

 - [Telecon minutes](telecons)

## Get help

 - [Frequently asked questions](FAQ)

 - Ask the _CosmoSIS_ users mailing list (see above)

 - Email the authors of the module you're using 

 - [Open an issue with CosmoSIS](https://bitbucket.org/joezuntz/cosmosis/issues/new)


