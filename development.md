# Development version

We add cosmosis changes and new features to a "development" version (or "branch") before merging them into a main numbered release.  You can use this version to get new features and bug fixes, but expect more bugs and other issues, which we'd be grateful if you'd [report to us](https://bitbucket.org/joezuntz/cosmosis/issues/new) - please let us know that you are using the development version.

# Getting the development version

This commands will take you from the master version to the current development version:

```
#!bash
update-cosmosis --develop
```

Or if you would like to do things manually:

```
#!bash
git checkout develop
git pull
cd cosmosis-standard-library
git checkout develop
git pull
cd ..
```

If you have made changes to the cosmosis code that would conflict with changes in the development version then this will refuse and give you a warning.  If you are happy to **lose those changes** then run:
```
#!bash
git checkout .
cd cosmosis-standard-library
git checkout .
cd ..
```
and then re-run the first set of commands.

# Changes in the current development version

## Samplers
 - Approximate Bayesian Computation (ABC) sampler


# Developer TODO:

Need module.yaml files for these:
 - supernovae/salt2
