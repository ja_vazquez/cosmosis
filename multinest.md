# Multinest

## Overview

Authors: Farhan Feroz, Mike Hobson, Michael Bridges

Website: http://ccpforge.cse.rl.ac.uk/gf/project/multinest/

Paper: http://arxiv.org/abs/0809.3437

Multinest is an implementation of an advanced form of [nested sampling](http://en.wikipedia.org/wiki/Nested_sampling_algorithm), in which an ensemble of live points is gradually shrunk towards the peak by proposing into a set of ellipses fit to the ensemble and accepting only new points with greater likelihood.  

Multinest can be run in parallel.

## Compiling and running

If a fortran MPI compiler is found during the build then it will be used to compile an MPI version of Multinest.  Otherwise a serial version will be built.  If you look closely in the output of "make" it tells you which.

You can run multinest in serial like this:

```
#!bash
cosmosis params.ini
```

or with MPI like this, for e.g. 4 processes.  Note that this is different to the other parallel samplers!:

```
#!bash
mpirun -n 4 cosmosis params.ini
```


## Options

Multinest is highly configurable and has a large number of options. The basic ones are described in this example ini file section; some others are described in [the multinest readme](https://bitbucket.org/joezuntz/cosmosis/src/b6b71f96096ec979e1e8ee96f0fa4baeb2dabe8c/cosmosis/samplers/multinest/multinest_src/README?at=master).

```
#!ini

[runtime]
sampler=multinest

[multinest]

; These are the only required parameters

; max_iterations is the total maximum number of iterations before finishing
; this value is enough for typical problems
max_iterations = 50000

; live points is the number of points in the ensemble.  A few hundred is typical; more points means slower convergence but higher accuracy
live_points = 400

; The remainder of these parameters have default values, which are shown here

;Whether to print output during sampling
feedback = True

; Get outputs in multinest's own format as well as from cosmosis using this filename base.
; Also enables restarting if the run is interrupted
multinest_outfile_root =

;Resume from a pre-exising run.  Requires a set (and unchanged from previous run) multinest_outfile_root
resume = F

;Whether to allow multiple ellipses to cover the samples
;Useful for banana-shaped spaces and other oddities
;and max number of such modes
mode_separation = False
max_modes = 100

; Random seed deciding sampling. If -1 set from time.
random_seed = -1

; Target error on Bayesian evidence
tolerance = 0.1

;Iterations before output
update_interval = 200


; See the link above for descriptions of these
importance = True
efficiency = 1.0
log_zero = 1e-6
const_efficiency = False
cluster_dimensions = -1
mode_ztolerance = 0.5
```
