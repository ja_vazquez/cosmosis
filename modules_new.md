# Importing existing code into CosmoSIS #

For all but the simplest modules it is best to separate your code into the part that does the actual calculations, and the part that connects it to cosmosis.  

Write your calculation code as a library - a collection of functions that can be called from the outside.  Then follow the instructions on [importing existing code](modules_legacy).