The pipeline is the core of the CosmoSIS system.  [Samplers](samplers) drive the calculation but interact with all other parts of CosmoSIS, and in particular the [physics and likelihood modules](modules), through the pipeline.  The following describes the configuration options used to configure the CosmoSIS pipeline so it can be used by samplers.

### Pipeline ###

The `[pipeline]` section lists the modules to load and execute, defines the input parameters and their priors, and one or more output likelihoods.

Example pipeline section:
```
#!ini

[pipeline]
modules = camb planck  ; a list of modules in the order they will be executed
values = values.ini    ; a single ini file containing parameter values and ranges
priors = priors.ini    ; a list of files containing parameter priors
extra_output =         ; optional list of computed items to save (e.g. sigma8, power spectrum)
likelihoods = planck   ; a list of modules that will compute likelihood values
quiet=T                ; silence extra output, default T
debug=F                ; turn on debugging output, default F
timing=F               ; output timing information for each module, default F
```

### Output ###

The output section is used to configure the output file format for samplers that support chain-style output.  Currently there are two output types, text and cosmomc.

### Modules ###

Each entry in the pipeline `modules` parameter has its own ini section with a single required parameter, `file`, which points to the module's location (typically a file ending in .so or .py).  The remaining parameters allow the module to specify the name of each of its [module functions](modules) (setup, execute, and cleanup), and then configure any custom parameters for that module, passed in the DataBlock to setup.

An example module section is as follows:

```
#!ini

[planck]
file = cosmosis-standard-library/likelihood/planck/planck_interface.so ; path to the Planck module
t_high_file = ups/planckdata/v1_1/data/CAMspec_v6.2TN_2013_02_26_dist.clik ; custom parameter to the Planck module pointing to the location of the high-ell temperature data
```