# Writing a module in C++ #

```
#!C++

#include "cosmosis/datablock/datablock.hh"
#include "cosmosis/datablock/section_names.h"
#include "my_calculation_code.h"

extern "C" {

void * setup(DataBlock * options)
{
	// Read options from the CosmoSIS configuration ini file,
	// passed via the "options" argument

	// Set any global variables required

	// Record any configuration information required

	// Pass back any object you like
}

DATABLOCK_STATUS execute(DataBlock * block, void * config)
{
	// Config is whatever you returned from setup above
	// Block is the collection of parameters and calculations for
	// this set of cosmological parameters

	DATABLOCK_STATUS status = 0;

	return status;
}


int cleanup(void * config)
{
	// Config is whatever you returned from setup above
	// Free it 
}

} // end of extern C

```
## Listing of cosmosis C++ methods for modules ##

The DataBlock class that you get passed in the setup and execute functions has these methods for loading and saving data:

