# Setting priors with the priors file #

All parameters have an automatic top-hat prior that is given by the range they take in the [values file](values_file).  But in addition to this you can add extra priors to individual parameters.

First, in your main [parameter file](parameter_file), in the ``[pipeline]`` section set the ``priors`` option to point to a new file, for example:

```
#!ini
; This would be in your parameter file
; ...
[pipeline]
priors = priors.ini
; ...
```

Then create that new file ``priors.ini``.  It has a similar form to the values file, with sections and parameters, but the values you given instead specify the prior on that parameter.  

You can specify gaussian, uniform, or exponential priors.  Here's how:

```
#!ini
; This would be in your new priors file

[cosmological_parameters]
; Gaussian prior - mean and standard deviation
omega_m = gaussian 0.3 0.05

; uniform prior - upper and lower values
n_s = uniform 0.9 1.0

; exponential prior, beta parameter; same units as parameter: (p ~ exp(-x/beta))
tau = exponential 0.05
```

These priors are in addition to the top hat priors already applied.  You don't have to specify priors for all the parameters - the rest will just use the automatic top hat.  You can also specify parameters that you never actually use in the values file in here (for example if you have several different analyses and some parameters are fixed in some versions).