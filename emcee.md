------------------------------------------------------------------------
# Emcee version 2.0 #

Authors: Daniel Foreman-Mackey, David W. Hogg, Dustin Lang, Jonathan Goodman

Website: http://dan.iel.fm/emcee/current/#

Documentation: http://arxiv.org/abs/1202.3665

emcee is a python implementation of an affine invariant Markov chain Monte Carlo (MCMC) ensemble sampler (Goodman & Weare 2010). The emcee sampler simultaneously evolves an ensemble of *walkers* where the proposal distribution of one walker is updated based on the position of all other walkers in a complementary ensemble.

The output will be (walkers*samples) number of steps for each parameter. 

# Usage

```
cosmosis [ini]
```
```
mpirun cosmosis --mpi [ini]
```

See [the page on MPI](mpi) for information on using MPI to do parallel sampling.


---------

## Options reference

The following options can be used in the emcee section

### *walkers*

Required integer number of walkers to use in ensemble. Minimum is 2*nparam+2, but more than that is usually better.  For a 6D space we usually use a few hundred.  Must be even to use MPI mode.  

### *samples*

Required integer number of steps each walker takes, the default  = 1000. 
total samples = walkers*samples

### *nsteps*

Required integer.  Sample points are output every nsteps, default = 100
since a single emcee step evaluates once for each walker
you should use a small number for this
nsteps = 1


### *start_points*

Optional string name of file containing contains at least (#walkers,#params) values to use as starting point for new chain.  You can use this to continue an emcee run from where it left off - use the old chain as the starting point for the new one.  If you do this then make sure to rename the file first, or change to a new output filename - otherwise it will be over-written.

### *random_start*

Optional Boolean T/F.  Ignored if start_file is set.  Otherwise if set to True, randomly scatter the walkers throughout the flat prior.  This is not usually a good idea unless you're really unsure about the starting point as the burn-in can be very slow.  The (default) alternative is to start from a small ball around the starting position in the values file.  If you do this then make sure the starting point is high likelihood - emcee has quite poor burn-in behaviour.  You can use the maxlike sampler to find a good point.

