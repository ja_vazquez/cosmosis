# CosmoSIS modules #

# An overview #

CosmoSIS is based on *modules*: separate, independent bits of code to do different bits of the physics and likelihood calculations.  The CosmoSIS pipeline runs a sequence of modules, passing the outputs of each on to the rest in the line.

A very simple pipeline might just have two modules; for example, the supernova model in [our demo 5](Demo5) has just two modules - one to get mu(z) and one to get the likelihood of mu.  More complex models with multiple systematics can be built out of more modules.

Splitting things up into discrete modules has several important advantages:

- Possible to use **multiple languages** more cleanly
- Ensure **consistency** when combining likelihoods
- Easier to **replace** part of the calculation
- Easier to **compare** two models
- Easier to **debug** things with clear inputs and outputs
- Easier to **configure** new and different pipelines without recompiling
- Can **collect** all your different analyses in a single directory.

To build up your pipelines you can either use the [modules supplied with CosmoSIS](default_modules) or [create new ones](creating_modules), perhaps by [modifying an existing one](modules_modify).


