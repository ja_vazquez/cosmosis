## 

If it is available for your system (Mavericks or SLF6) we strongly recommend you use the [automatic bootstrap installation script](bootstrap).

The things it installs are kept neatly in a single directory and will not mess up your current installation of anything.  Even if you're an expert it really is much easier.

# 1 Dependencies

If you can't or don't want to use the bootstrap method then you can manually install CosmoSIS.  There are quite a number of requirements that you must have before you can start:

- [gcc/g++/gfortran 4.8 or 4.9](http://ftpmirror.gnu.org/gcc/gcc-4.9.3/)  (NB there is a known gcc 4.8 bug on some ubuntu systems)
- [gsl 1.16](http://ftpmirror.gnu.org/gsl/) or above
- [cfitsio 3.30](http://heasarc.gsfc.nasa.gov/fitsio/fitsio.html) or above
- [fftw 3](http://www.fftw.org/download.html)
- [lapack](http://www.netlib.org/lapack/#_lapack_version_3_5_0) (except on a mac)
- [git](https://git-scm.com/downloads)

Python things.  You can get all these with the [conda distribution](https://store.continuum.io/cshop/anaconda/)
which we recommend:

- [python 2.7](https://www.python.org/downloads/release/python-2710/)
- [numpy](http://www.scipy.org/scipylib/download.html)
- [scipy](http://www.scipy.org/scipylib/download.html)
- [nose](https://pypi.python.org/pypi/nose/)
- [pyyaml](http://pyyaml.org/wiki/PyYAML)
- [matplotlib](http://matplotlib.org/downloads.html) (optional; for plotting results)

- [planck data](http://pla.esac.esa.int/pla/) (optional; for some demos and for using Planck likelihoods.  Go to the "cosmology" link)
- [wmap data](http://lambda.gsfc.nasa.gov/product/map/dr5/likelihood_get.cfm) (optional; for using WMAP likelihoods)


To use the emcee sampler you also need the "emcee" python package - if you used conda python you can do that with:

    conda install emcee

or otherwise with:

    pip install emcee

or if that doesn't work:

    easy_install emcee

If these fail with permission errors then add the "--user" flag to put them in your home dir instead of a global one.


You need to ensure in particular that the scipy and numpy installations used the same compilers you use with CosmoSIS.

# 2 Download

Download CosmoSIS and the standard library like this (will get you v1.0; leave out the checkouts for bleeding edge version):

```
#!bash
git clone http://bitbucket.org/joezuntz/cosmosis
cd cosmosis
git checkout
git clone http://bitbucket.org/joezuntz/cosmosis-standard-library
cd cosmosis-standard-library
git checkout
cd ..
```

# 3 Setup script

From the cosmosis directory make a copy of the manual setup script:

    cp config/manual-install-setup setup-my-cosmosis

**Edit the new file setup-my-cosmosis** and replace all the places where it says /path/to/XXX in this file with correct paths to the directory containing XXX.

# 4 Build

Source the setup script and make:

    source setup-my-cosmosis
    make

If you get any errors please send your setup-my-cosmosis script and the complete output of "make" to the cosmosis developers.  You can do this via the "Issues" tab to the right.

#5 Usage

If you quit your terminal shell and start a new one then you need to repeat this step:

    source setup-my-cosmosis

Then test your install is working by [running Demo 1](Demo1).
