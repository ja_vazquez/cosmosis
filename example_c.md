# Example C:  Sampling a maximum cluster mass likelihood #

## Running ##

In this example we generate the maximum cluster mass likelihood at z=1.6 using the emcee sampler run in parallel, and data from Santos et al 2011 (Table 1 of Harrison & Coles 2012).

Run the demo on 4 processors using:


```
#!bash
source config/setup-cosmosis

mpirun -n 4 cosmosis --mpi demos/example_c.ini
```

This is a longer example that will take about an hour to run. You can generate the plots of the output using

```
#!bash
postprocess -o plots -p example_c demos/example_c.ini --burn 1000

```

![example_c_2D_cosmological_parameters--omega_m_clusters--m_max.png](https://bitbucket.org/repo/KdA86K/images/581724006-example_c_2D_cosmological_parameters--omega_m_clusters--m_max.png)

You will get some summary statistics, as with the other samplers in [demos three](Demo3), [five](Demo5) and [nine](Demo9), for example:

```
#!text
Marginalized mean, std-dev:
    cosmological_parameters--omega_m = 0.258313 ± 0.0282911
    clusters--m_max = 2.55063e+14 ± 6.13247e+13
    like = -66.3245 ± 0.979674

Marginalized median, std-dev:
    cosmological_parameters--omega_m = 0.258094 ± 0.0282911
    clusters--m_max = 2.49112e+14 ± 6.13247e+13
    like = -66.035 ± 0.979674

Best likelihood:
    cosmological_parameters--omega_m = 0.26646
    clusters--m_max = 2.71106e+14
    like = -65.2116

```

## Understanding ##

### The sampling ###
 
We are now using a proper MCMC sampler, called emcee.  This sampler is described in http://arxiv.org/abs/1202.3665 .
See demo 5 on the wiki for a shorter example of how to use this sampler. In this run we are using 16 walkers and 200 samples which should be enough as we are only varying two parameters M_max and Omega_m.

The pipeline for this run is the same as in demo 13

```
#!bash
[pipeline]
modules = consistency camb mf_tinker evs cluster_mass
likelihoods = evs maxmass
``` 

In this example we are running the "evs" module with "output_pdf = F" to speed up the sampling time. See demo 13 for more details on each of the modules in the pipeline.
In this example we are allowing the M_max and omega_m parameters to vary in fitting to the Santos et al 2011 cluster mass measurement at z=1.5.

If you want to edit this demo to output likelihoods for a cluster mass at another redshift you can do this by modifying the .ini file for camb, evs and cluster_mass modules. Let's say we have some cluster mass data like Menanteau et al 2011 's "El Gordo" at z=0.87 then we need to output power spectra from camb in a redshift range around this target and get the max mass PDF using

```
#!bash
[camb]
file = cosmosis-standard-library/boltzmann/camb/camb.so
zmin = 0.5
zmax = 1.0
nz = 40

[evs]
file =cosmosis-standard-library/likelihood/cluster_evs/cluster_evs.py
; Redshift of observed cluster
redshift = 0.9
;Fraction of sky observed to get cluster
frac = 1.0
output_pdf = F

```

We can input the cluster mass data in the .ini file as

```
[cluster_mass]
file = cosmosis-standard-library/likelihood/cluster_mass/cluster_mass.py
;See Menanteau et al 2011, Harrison & Coles 2012 Table 1 (masses converted to M_sun/h)
mean = 1.295e+15
sigma =  2.94e+14
```
At this lower redshift we find that the mean maximum cluster mass is higher at around 1.1E15 M_sun/h

![gordo_2D_cosmological_parameters--omega_m_clusters--m_max.png](https://bitbucket.org/repo/KdA86K/images/1970145267-gordo_2D_cosmological_parameters--omega_m_clusters--m_max.png)