-----------------------------------------------

# Grid Sampler

The Grid Sampler is used to sample the CosmoSIS parameters in a regularly spaced set of points, or grid. This is an efficient way to explore the likelihood functions and gather basic statistics, particularly when only a few parameters are varied.  When the number of parameters is large, the number of sampled points in each dimension must necessarily be kept small.  This can be mitigated somewhat if the grid is restricted to parameter ranges of interest.  An example of using the Grid Sampler to sample a single parameter is given in [Demo 3](Demo3), and a 2D example in [Demo 7](Demo7).

#Usage
```
cosmosis [ini]
```
```
mpirun cosmosis --mpi [ini]
```

# ini file options

The Grid Sampler uses a standard CosmoSIS _ini_ file with [pipeline], [output] and [module] interface sections together with the following [runtime] and [grid] sections:
```
[runtime]
sampler = grid

[grid]
nsample_dimension=[integer]
```
where nsample_dimension is the number of points sampled in each parameter dimension.