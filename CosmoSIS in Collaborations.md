#Using CosmoSIS in a collaboration

CosmoSIS can be used to coordinate the development of parameter estimation
codes within the large collaborations that are the norm for modern cosmology.
Doing so requires finding a happy medium between the more careful development
approach one should take for releasing code to the wider community and the 
free-wheeling way one can work alone or in tight-knit groups.

This short note recommends an approach for this situation. 
If you have any thoughts to add then please feel free.


##General Structure

A collaboration should have a single private git repository for cosmological 
parameter estimation, with a hierarchical structure inside (for example by 
working group).

For each project or activity (loosely defined) within the collaboration:

- analysis should be split up into separate cosmosis modules as far as you can.
    - **in particular** you should definitely split theory prediction code ("physics modules") into separate modules from comparisons to data (e.g. chi^2), since then others can use one without the other.
    - each module should have one directory, organized by theme and not project.
        - for example, collaboration/likelihoods/project_name 
          NOT collaboration/project_name/likelihood.
- libraries used by several modules should have their own directory.
- there should be a directory collaboration/projects/project_name, just for
  the ini files to make the project results, with very brief notes on how to
  use them.  Anyone should be able to re-create plots you make.


##Writing Modules
- Don't be shy about creating new modules.
- Wherever possible use inputs (like P(k,z), D(z), etc.) from the standard
   library or other modules. Don't re-invent the wheel.
- Write a module.yaml file describing the module and its jobs and limitations.
    - Keep it up to date when you change things!
    - Include (in the yaml or elsewhere) a note saying who wrote the module.

##Sharing & Updating Modules

- You're always allowed to change your own module code however you like
- You're always allowed to use someone else's module in your project

- If you change a module that others may be using:
    - Tell them!
    - be as clear as possible - put a note in the readme explaining the changes as best you can
    - if you change what an output means then change its name (e.g. sigma_R -> sigma_Rz)
    - if you change what inputs a module needs then make sure the error message 
     is clear if it is missing and that this is a new change

- If you use someone else's module:
    - Tell them!
    - Be aware they may change it; keep track of the logs
    - Be especially aware that the ranges that a module outputs over are
      especially likely to change (for example they might change the 
      redshift limits of their outputs).  Have your code check what you
      are expecting

- If you want to extend or modify someone else's module:
    - If the change should be generally useful, push it to a branch and ask that
      they merge it into their module.  Ideally keep the default behaviour and
      make the change happen only if an ini file option is set
    - If it's just for your specific application, consider making a new module
      that calls the same underlying code; you are then responsible for this new
      module.

- If a module is "stale" and you have a new, better version, then delete the 
  old one.  It can always be recovered.

##Version Control
- Version control with git is fiddly but quite well documented.
- Use a GUI like SourceTree.  Much, much, easier to check what you've done.
- Use "git status" to check what the status is at any time.
- If you're stuck google your problem and click the first link to
  wwww.stackoverflow.com that comes up.
- If you're clever enough to do cosmology then you're clever enough to use
   git.
