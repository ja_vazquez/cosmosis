# Debugging C/C++/Fortran modules #

Modules written in C, C++, Fortran, or other compiled languages can be debugged using standard debuggers like **gdb** or **lldb**.

These debuggers are an alternative to peppering your code with print statements - they let you stop the code at a particular point and see what values variables have.  They are especially useful if your code crashes with a "segmentation fault" or similar error, because they will stop automatically and you can see what happened.

They tend to work much better for C and C++ than Fortran, and are not suited at all to debugging python - there is a python package called pdb for that.

First, check that you have one of these debuggers:

    which lldb
    which gdb

if one of these commands prints out a path to a debugger then you have that one installed. If not then you can get the debuggers from a package manager.  

*NB*: The gdb that comes with the cosmosis auto-install does not work on a mac without heroically painful work.  We strongly recommend lldb on macs.

## Compiling for debuggability ##

You can tell cosmosis to compile everything in debug mode by setting an environment variable when you run make:

    make clean
    COSMOSIS_DEBUG=1 make

The code will now be compiled in a way that it can be run under the debugger.  This will make the code slower, so remember to make clean and make again once you've finished debugging.

## Running under lldb ##

Run under lldb like this:

    lldb python bin/cosmosis params.ini


Then start the program with:

    (lldb) run

## Running under gdb ##

Run under gdb like this:

    gdb python

Then start the program with:

    (gdb) set args  bin/cosmosis params.ini
    (gdb) run

## Debugging

The program will now run as normal, but on a crash will stop and drop you into a debug environment where you can use commands like "up" "down" and "print" to move up and down the program and print out variables.  You may need to go "up" a few times from where the crash actually happens.

A full tutorial on this is beyond the scope of this page - please see [this gdb tutorial](http://www.unknownroad.com/rtfm/gdbtut/gdbtoc.html) and [this limited lldb tutorial](http://lldb.llvm.org/tutorial.html).

